import operator
import gzip
from os import listdir
from os.path import isfile, join
import json
from pymongo import MongoClient
import pymongo
import networkx as nx
import timeit
from networkx.algorithms import approximation as apxa
import sys, string, os
from subprocess import call
import csv
from bson.objectid import ObjectId
import collections
import  Queue as queue
import  math
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import scipy as sp
import pylab
import community
import hashlib
from os import listdir
from os.path import isfile, join
import re
import shutil
import random
import snap
import time
from datetime import datetime
from matplotlib import pyplot as plt
import matplotlib as mp
import pandas as pd
import pylab as pl
import scipy.cluster.hierarchy as sch
import string
import matplotlib.colors as mcol
from sklearn.datasets import make_biclusters
from sklearn.datasets import samples_generator as sg
from sklearn.cluster.bicluster import SpectralCoclustering
from sklearn.metrics import consensus_score
#import heatmap_plot as hm
import traceback
import shutil
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn import cross_validation
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.cluster import KMeans
from sklearn.cross_validation import cross_val_predict
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import average_precision_score
from sklearn.metrics import accuracy_score
from matplotlib import rc,rcParams
from random import randrange

class MyLogisticRegression(object):
    def __init__(self, num_features):
        print 'here'
        self.num_features = num_features


    def loss(self, X, y, theta):
        '''
        Find loss and grads of logistic funciton -\sum_{i=1}^{N} { y^i*log(h(x^i))+(1-y^i)*(1-log(h(x^i))  }
        :param X: shape (N,D) in R^(NxD) D is number of features
        :param y: shape (N,)
        :param theta: shape (D,)
        :return: loss and grads.
        '''
        loss = None
        assert type(X) == np.ndarray
        X=X.astype(float)
        N,D = X.shape
        assert theta.shape[0] == D
        assert y.shape[0] == N
        #T=X.dot(theta) #in R^{N x 1}
        sigmod=1.0/(1+np.exp(-X.dot(theta[np.newaxis,:].T)))+1e-5
        for e in sigmod:
            assert e>=0, 'e is: %s' % e
        T=y[np.newaxis,:].T*(1-sigmod) - (1-y[np.newaxis,:].T)*sigmod #\in R^{M x 1}
        T = y[np.newaxis,:]-sigmod
        #print T
        grads=X.T.dot(T) # R^{D x 1}
        grads=grads.T[0]
        y_ = y[np.newaxis,:].T
        # print sigmod
        # print 1-sigmod
        loss = -np.sum(y_*np.log(sigmod)+(1-y_)*np.log(1-sigmod)) # Adding (-) minus to make loss function convex based on theta.
        return loss, -grads #grad should be (-) since we just add minis above

    def train(self, X, y, num_iters=100, batch_size=5, learning_rate=1e-1):
        assert X.shape[1] == self.num_features
        N, D = X.shape
        theta = None
        if theta == None:
            theta = 0.001 * np.random.randn(self.num_features)
        #num_iters = 100
        #batch_size = 5
        # learning_rate = 1e-1
        loss_history = []
        # print X,y
        for it in xrange(num_iters):
            randInds = np.random.choice(N, batch_size)
            X_batch = X[randInds, :]
            y_batch = y[randInds]
            assert X_batch.shape[0] == batch_size
            assert X_batch.shape[1] == D
            assert y_batch.shape[0] == batch_size

            loss, dTheta = self.loss(X, y, theta=theta)  # It is quite dump, since I used all of them

            theta += -learning_rate * dTheta
            # print theta
            loss_history.append(loss)

            # if verbose and it % 5 == 0:
            print 'iteration %d / %d: loss %f' % (it, num_iters, loss)

        plt.plot(np.arange(len(loss_history)), loss_history, 'r--')
        plt.xlabel('iterations')
        plt.ylabel('loss')
        plt.show()
        print 'final coefficient: ', theta

def findMin():
    '''
    finding min of convex function using Newton method. x_{k+1}=x_k- g'(x_k) / g''(x_{k+1})
    :return:
    '''
    x=-50
    g=lambda x: pow((x+2), 4)-2
    dg=lambda x: 4*pow((x+2),3)
    ddg=lambda x: 12*pow((x+2),2)
    niter=100
    pairs=[]
    x_=[x]
    y_=[g(x)]
    for i in range(niter):
        x=x-dg(x)/float(ddg(x))
        x_.append(x)
        y_.append(g(x))
        # print x_, y_
    assert len(x_) == niter+1
    assert len(y_) == niter+1
    x_=np.array(x_)
    y_=np.array(y_)
    plt.plot(x_, y_, 'r-')
    plt.xlabel('x')
    plt.ylabel('(x+2)^4-2')
    plt.show()

def findMax():
    '''
    finding min of convex function using Newton method. x_{k+1}=x_k- g'(x_k) / g''(x_{k+1})
    It is exactly same.
    :return:
    '''
    x = -50
    g = lambda x: -pow((x + 2), 4) - 2
    dg = lambda x: -4 * pow((x + 2), 3)
    ddg = lambda x: -12 * pow((x + 2), 2)
    niter = 100
    pairs = []
    x_ = [x]
    y_ = [g(x)]
    for i in range(niter):
        x = x - dg(x) / float(ddg(x))
        x_.append(x)
        y_.append(g(x))
        # print x_, y_
    assert len(x_) == niter + 1
    assert len(y_) == niter + 1
    x_ = np.array(x_)
    y_ = np.array(y_)
    plt.plot(x_, y_, 'r-')
    plt.xlabel('x')
    plt.ylabel('-(x+2)^4-2')
    plt.show()


def fit(X, y, theta=None):
    '''Fully vectorized'''
    assert type(X) == np.ndarray
    N, D = X.shape
    assert len(y) == N
    assert type(y) == np.ndarray

    # compute loss function with respect to theta
    loss = None

    T = X * y[np.newaxis,:].T
    yThetaX=T.dot(theta[np.newaxis,:].T) #\in R^{N x 1}
    assert yThetaX.shape[0] == N
    assert yThetaX.shape[1] == 1
    hThetaX=1.0/(1+np.exp(-1*yThetaX)) #\in R^{N x 1}
    loss = -1.0/N * np.sum(np.log(hThetaX))

    assert hThetaX.shape == yThetaX.shape
    IM = T*(1-hThetaX)
    dTheta=-1.0/N * np.sum(IM, axis=0) #sum theo column


    return loss, dTheta

def fitNewton(X, y, theta):
    '''
    theta_new = theta_old - J'(theta) / J''(theta)
    :param X:
    :param y:
    :param theta:
    :return:
    '''
    assert type(X) == np.ndarray
    N, D = X.shape
    assert len(y) == N
    assert type(y) == np.ndarray

    # compute loss function with respect to theta
    loss = None

    T = X * y[np.newaxis, :].T
    yThetaX = T.dot(theta[np.newaxis, :].T)  # \in R^{N x 1}
    assert yThetaX.shape[0] == N
    assert yThetaX.shape[1] == 1
    hThetaX = 1.0 / (1 + np.exp(-1 * yThetaX))  # \in R^{N x 1}
    loss = -1.0 / N * np.sum(np.log(hThetaX))

    assert hThetaX.shape == yThetaX.shape
    IM = T * (1 - hThetaX)
    dTheta = -1.0 / N * np.sum(IM, axis=0)  # sum theo column \in R^{D x 1}

    T2=T*T * hThetaX * (1-hThetaX)
    d2Theta = 1.0/N * np.sum(T2, axis=0)
    assert d2Theta.shape == dTheta.shape
    theta +=  -dTheta / (d2Theta+1e-10)
    return loss, theta

def trainSGD(verbose=True):
    '''
    Using stochastic gradient descent to find theta.
    :param verbose:
    :return:
    '''
    fin=open('D:/USU/CS229_Stanford_ML/hw1/logistic_x.txt', 'rb')
    X = []
    for line in fin:
        x1,x2=line.split()
        x1=float(x1)
        x2=float(x2)
        X.append([x1, x2, 1])
    X=np.array(X)
    assert X.shape[0] == 99
    assert X.shape[1] == 3
    N,D = X.shape

    fin=open('D:/USU/CS229_Stanford_ML/hw1/logistic_y.txt', 'rb')
    y=[]
    for line in fin:
        yi=float(line)
        y.append(yi)
    y=np.array(y)
    assert y.shape[0] == 99
    theta=None
    if theta == None:
        theta = 0.001 * np.random.randn(D)
    num_iters=100
    batch_size = 5
    learning_rate=1e-1
    loss_history = []
    # print X,y
    for it in xrange(num_iters):
        randInds = np.random.choice(N, batch_size)
        X_batch = X[randInds, :]
        y_batch = y[randInds]
        assert X_batch.shape[0] == batch_size
        assert X_batch.shape[1] == D
        assert y_batch.shape[0] == batch_size

        loss, dTheta=fit(X, y, theta=theta) #It is quite dump, since I used all of them
        theta += -learning_rate*dTheta
        loss_history.append(loss)

        # if verbose and it % 5 == 0:
        print 'iteration %d / %d: loss %f' % (it, num_iters, loss)

    plt.plot(np.arange(len(loss_history)), loss_history, 'r--')
    plt.xlabel('iterations')
    plt.ylabel('loss')
    plt.show()
    print 'final coefficient: ', theta



def trainUsingNewTonMethod():
    '''
    theta = theta - J'(theta) / J''(theta).
    :return:
    '''
    fin = open('D:/USU/CS229_Stanford_ML/hw1/logistic_x.txt', 'rb')
    X = []
    for line in fin:
        x1, x2 = line.split()
        x1 = float(x1)
        x2 = float(x2)
        X.append([x1, x2, 1])
    X = np.array(X)
    assert X.shape[0] == 99
    assert X.shape[1] == 3
    N, D = X.shape

    fin = open('D:/USU/CS229_Stanford_ML/hw1/logistic_y.txt', 'rb')
    y = []
    for line in fin:
        yi = float(line)
        y.append(yi)
    y = np.array(y)
    assert y.shape[0] == 99
    theta = None
    if theta == None:
        theta = 0.001 * np.random.randn(D)
    num_iters = 100
    batch_size = 5
    learning_rate = 1e-1
    loss_history = []
    # print X,y
    for it in xrange(num_iters):
        randInds = np.random.choice(N, batch_size)
        X_batch = X[randInds, :]
        y_batch = y[randInds]
        assert X_batch.shape[0] == batch_size
        assert X_batch.shape[1] == D
        assert y_batch.shape[0] == batch_size

        loss, theta = fitNewton(X, y, theta=theta)  # It is quite dump, since I used all of them
        # theta += -learning_rate * dTheta
        loss_history.append(loss)

        # if verbose and it % 5 == 0:
        print 'iteration %d / %d: loss %f' % (it, num_iters, loss)

    plt.plot(np.arange(len(loss_history)), loss_history, 'r--')
    plt.xlabel('iterations')
    plt.ylabel('loss')
    plt.show()
    print 'final coefficient: ', theta
    plt.clf()
    # x1=X[0, :]
    # x2=X[1, :]
    red_idx=np.argwhere(y>0)
    blue_idx=np.argwhere(y<0)
    # print red_idx
    # print blue_idx
    red_idx = red_idx.T[0]
    # print red_idx
    blue_idx = blue_idx.T[0]
    X = X[:, 0:2]
    # print K
    # reds_pnt=X[red_idx, [0 for i in range(len(red_idx))]]
    reds_pnt=np.array([X[ind] for ind in red_idx])
    blues_pnt = np.array([X[ind] for ind in blue_idx])
    # blues_pnt=X[blue_idx, [1 for i in range(len(blue_idx))]]
    # print reds_pnt
    plt.plot(reds_pnt[:,0], reds_pnt[:,1], 'r*')
    plt.plot(blues_pnt[:, 0], blues_pnt[:,1], 'b+')
    #plot line
    x1=np.arange(10)
    x2=(-theta[2]-theta[0]*x1)/theta[1]
    plt.plot(x1, x2, ls='-', c='r')
    plt.show()

def readData():
    fin = open('D:/USU/CS229_Stanford_ML/hw1/logistic_x.txt', 'rb')
    X = []
    for line in fin:
        x1, x2 = line.split()
        x1 = float(x1)
        x2 = float(x2)
        X.append([x1, x2, 1])
    X = np.array(X)
    assert X.shape[0] == 99
    assert X.shape[1] == 3
    N, D = X.shape

    fin = open('D:/USU/CS229_Stanford_ML/hw1/logistic_y.txt', 'rb')
    y = []
    for line in fin:
        yi = float(line)
        y.append(yi)
    y = np.array(y)
    assert y.shape[0] == 99
    return X, y


def eval_numerical_gradient(f, x, verbose=True, h=0.00001):
    """
    a naive implementation of numerical gradient of f at x
    - f should be a function that takes a single argument
    - x is the point (numpy array) to evaluate the gradient at
    """

    fx = f(x)  # evaluate function value at original point
    grad = np.zeros_like(x)
    # iterate over all indexes in x
    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:

        # evaluate function at x+h
        ix = it.multi_index
        # print ix, ' Index'
        oldval = x[ix]
        x[ix] = oldval + h  # increment by h
        fxph = f(x)  # evalute f(x + h)
        x[ix] = oldval - h
        fxmh = f(x)  # evaluate f(x - h)
        x[ix] = oldval  # restore
        # print 'old +h: %s' % fxph, 'new value -h: %s' % fxmh
        # compute the partial derivative with centered formula
        grad[ix] = (fxph - fxmh) / (2 * h)  # the slope
        if verbose:
            print ix, grad[ix]
        it.iternext()  # step to next dimension

    return grad
def rel_error(x, y):
  """ returns relative error """
  return np.max(np.abs(x - y) / (np.maximum(1e-8, np.abs(x) + np.abs(y))))

def my_grad_check(f, x, analytic_grad, num_checks=10, h=1e-5):
   """
      sample a few random elements and only return numerical
      in this dimensions.
   """
   print x.shape
   print analytic_grad.shape
   #return
   for i in xrange(num_checks):
    ix = tuple([randrange(m) for m in x.shape])
    #ix = randrange(x.shape[0])
    #print ix, 'dkm'
    #return
    oldval = x[ix]
    #print oldval
    #return
    #print 'Shape input:', oldval.shape
    x[ix] = oldval + h # increment by h
    fxph = f(x) # evaluate f(x + h)
    x[ix] = oldval - h # increment by h
    fxmh = f(x) # evaluate f(x - h)
    x[ix] = oldval # reset
    #print fxph, 'old value'
    #print fxmh, 'new value'
    grad_numerical = (fxph - fxmh) / (2 * h)
    grad_analytic = analytic_grad[ix]
    #print grad_analytic, 'here'
    #print grad_numerical, 'here2'
    assert grad_numerical.shape == grad_analytic.shape, 'Mismatched shape %s %s' % (grad_numerical.shape, grad_analytic.shape)
    rel_error = abs(grad_numerical - grad_analytic) / (abs(grad_numerical) + abs(grad_analytic))
    print 'numerical: %f analytic: %f, relative error: %f' % (grad_numerical, grad_analytic, rel_error)
def test_gradient():
    X, y = readData()
    m = MyLogisticRegression(num_features=3)

    theta = 0.001 * np.random.randn(3)
    loss, grads=m.loss(X, y, theta=theta)
    f=lambda x: m.loss(X, y, theta=theta)[0]
    # eval_numerical_gradient(f, theta)

    param_grad_num = eval_numerical_gradient(f, theta, verbose=False)
    print '%s max relative error: %e' % ('X-matrix as params', rel_error(param_grad_num, grads))
    my_grad_check(f, theta, grads)

def test_cvx():
    # Figure 4.11, page 185.
    # Regularized least-squares.

    from pickle import load
    from cvxopt import blas, lapack, matrix, solvers
    solvers.options['show_progress'] = 0

    data = load(open("rls.bin", 'rb'))
    A, b = data['A'], data['b']
    m, n = A.size

    # LS solution
    xls = +b
    lapack.gels(+A, xls)
    xls = xls[:n]

    # We compute the optimal values of
    #
    #     minimize/maximize  || A*x - b ||_2^2
    #     subject to         x'*x = alpha
    #
    # via the duals.
    #
    # Lower bound:
    #
    #     maximize    -t - u*alpha
    #     subject to  [u*I, 0; 0, t] + [A, b]'*[A, b] >= 0
    #
    # Upper bound:
    #
    #     minimize    t + u*alpha
    #     subject to  [u*I, 0; 0, t] - [A, b]'*[A, b] >= 0.
    #
    # Two variables (t, u).

    G = matrix(0.0, ((n + 1) ** 2, 2))
    G[-1, 0] = -1.0  # coefficient of t
    G[: (n + 1) ** 2 - 1: n + 2, 1] = -1.0  # coefficient of u
    h = matrix([[A.T * A, b.T * A], [A.T * b, b.T * b]])
    c = matrix(1.0, (2, 1))

    nopts = 40
    alpha1 = [2.0 / (nopts // 2 - 1) * alpha for alpha in range(nopts // 2)] + \
             [2.0 + (15.0 - 2.0) / (nopts // 2) * alpha for alpha in
              range(1, nopts // 2 + 1)]
    lbnds = [blas.nrm2(b) ** 2]
    for alpha in alpha1[1:]:
        c[1:] = alpha
        lbnds += [-blas.dot(c, solvers.sdp(c, Gs=[G], hs=[h])['x'])]

    nopts = 10
    alpha2 = [1.0 / (nopts - 1) * alpha for alpha in range(nopts)]
    ubnds = [blas.nrm2(b) ** 2]
    for alpha in alpha2[1:]:
        c[1:] = alpha
        ubnds += [blas.dot(c, solvers.sdp(c, Gs=[G], hs=[-h])['x'])]

    try:
        import pylab
    except ImportError:
        pass
    else:
        pylab.figure(1, facecolor='w')
        pylab.plot(lbnds, alpha1, 'b-', ubnds, alpha2, 'b-')
        kmax = max([k for k in range(len(alpha1)) if alpha1[k] <
                    blas.nrm2(xls) ** 2])
        pylab.plot([blas.nrm2(b) ** 2] + lbnds[:kmax] +
                   [blas.nrm2(A * xls - b) ** 2], [0.0] + alpha1[:kmax] +
                   [blas.nrm2(xls) ** 2], '-', linewidth=2)
        pylab.plot([blas.nrm2(b) ** 2, blas.nrm2(A * xls - b) ** 2],
                   [0.0, blas.nrm2(xls) ** 2], 'bo')
        pylab.fill(lbnds[-1::-1] + ubnds + [ubnds[-1]],
                   alpha1[-1::-1] + alpha2 + [alpha1[-1]], facecolor='#D0D0D0')
        pylab.axis([0, 15, -1.0, 15])
        pylab.xlabel('||A*x-b||_2^2')
        pylab.ylabel('||x||_2^2')
        pylab.grid()
        pylab.title('Regularized least-squares (fig. 4.11)')
        pylab.show()


        
if __name__ == '__main__':
    'TODO'
    # findMin()
    # findMax()
    # trainSGD()
    # trainUsingNewTonMethod()
    # X,y = readData()
    #
    # m = MyLogisticRegression(num_features=3)
    # m.train(X, y)
    # test_gradient()
    test_cvx()