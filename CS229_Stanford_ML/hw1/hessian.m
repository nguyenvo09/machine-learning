function T = hessian(X, theta, y)

m = length(X);
n = length(theta);
H = zeros(n,n);
for i=1:m
    x = X(i,:)*theta*y(i,:);
    M_i = zeros(n,n);
    for j=1:n
        M_i(j,:)=X(i,:)*X(i,j);
    end
    M_i = sigmod(x) * (1-sigmod(x)) * M_i;
    H = H + M_i;
end
H = 1/m*H;
T = inv(H);

