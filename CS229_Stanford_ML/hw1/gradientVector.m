function gradVector = gradientVector(X, theta, y)

m = length(X);
n = length(theta);
temp = zeros(n,1);
for i =1:m
    for j=1:n
        x = X(i,:)*theta*y(i,:);
        temp(j) = temp(j) + (1-sigmod(x))*X(i,j);
    end
end
gradVector = temp*(-1/m);