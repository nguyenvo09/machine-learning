X = load('logistic_x.txt');
y = load('logistic_y.txt');

m = length(y); % number of training examples
n = size(X,2);
theta = zeros(n, 1);
n_theta = newton(2, X, theta, y);
disp(n_theta);

XX = horzcat(X, y);
n = size(XX,2);
XX1 = XX(XX(:,n)>0, :);
disp(XX1);
scatter(XX1(:,1),XX1(:,2),'+r');
hold on;
XX1 = XX(XX(:,n)<0, :);
disp(XX1);
scatter(XX1(:,1),XX1(:,2),'*g');
hold on;
