function new_theta = newton(num_iter, X, theta, y)
n = length(theta)
for i=1:num_iter
    H_inv = hessian(X,theta, y);
    grad_v=gradientVector(X,theta,y);
    theta=theta-H_inv*grad_v;

end
new_theta = theta;

