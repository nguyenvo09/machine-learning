import numpy as np
import random

from q1_softmax import softmax
from q2_sigmoid import sigmoid, sigmoid_grad
from q2_gradcheck import gradcheck_naive


def affine_forward(x, w, b):
    ''' Implement affine forward
        x \in (N,D), w \in (D,K), b \in (K,)'''
    x=x.astype(float)
    w=w.astype(float)
    b=b.astype(float)
    N,D=x.shape
    assert w.shape[0] == D
    assert b.shape[0] == w.shape[1]
    fc=x.dot(w)+b
    cache=(x,w,b)
    return fc, cache
    
def affine_backward(grad, cache):
    ''' Implement affine backward. 
        grad \in (N, K)
        x \in (N,D), w \in (D,K), b \in (K,)'''
    x,w,b=cache
    x=x.astype(float)
    w=w.astype(float)
    b=b.astype(float)
    N,D=x.shape
    K=b.shape[0]
    assert grad.shape == (N, K)
    dw=x.T.dot(grad) #(D,K)
    db=np.sum(grad, axis=0) #sum theo column
    dx=grad.dot(w.T) # (N, D)
    return dx, dw, db

def softmax_forward(x):
    return softmax(x)

def sigmoid_forward(x):
    cache=x
    return sigmoid(x), cache
def sigmoid_backward(grad, cache):
    x = cache
    v=sigmoid(x)
    
    return grad * v*(1-v)
def affine_sigmoid_forward(x, w, b):
    fc, cache_fc = affine_forward(x, w, b)
    out, cach_sigmoid=sigmoid_forward(fc)
    return out, (cache_fc, cach_sigmoid)
def affine_sigmoid_backward(grad, cache):
    cache_fc, cach_sigmoid = cache
    grad1=sigmoid_backward(grad, cach_sigmoid)
    return affine_backward(grad1, cache_fc)
def cross_entropy_softmax(X, y):
    y_hat=softmax(X)
    assert y_hat.shape == y.shape
    loss=-np.sum(y*np.log(y_hat))
    sm=np.sum(y, axis=1)
    sm=sm[np.newaxis,:].T
    dX = sm*y_hat - y
    return loss, dX
def forward_backward_prop(data, labels, params, dimensions):
    """ 
    Forward and backward propagation for a two-layer sigmoidal network 
    
    Compute the forward propagation and for the cross entropy cost,
    and backward propagation for the gradients for all parameters.
    """

    ### Unpack network parameters (do not modify)
    ofs = 0
    Dx, H, Dy = (dimensions[0], dimensions[1], dimensions[2])
    assert data.shape[1] == Dx
    
    X = data.astype(float) # \in R^{N x Dx}
    labels = labels.astype(float)
    N, Dx = X.shape
    
    assert labels.shape == (N, Dy)
    W1 = np.reshape(params[ofs:ofs+ Dx * H], (Dx, H))
    ofs += Dx * H
    b1 = np.reshape(params[ofs:ofs + H], (1, H))
    ofs += H
    W2 = np.reshape(params[ofs:ofs + H * Dy], (H, Dy))
    ofs += H * Dy
    b2 = np.reshape(params[ofs:ofs + Dy], (1, Dy))
    assert b1[0].shape == (H, )
    
    out1, cache1 = affine_sigmoid_forward(X, W1, b1[0])
    #cache_fc, cache_sigmoid = cache1
    out2, fc_2 = affine_forward(out1, W2, b2[0])
    loss, dout2 = cross_entropy_softmax(out2, labels)
    cost = loss
    dX2, dW2, db2 = affine_backward(dout2, fc_2)
    dX1, dW1, db1 = affine_sigmoid_backward(dX2, cache1)
    #raise NotImplementedError
    ### END YOUR CODE
    gradW1 = dW1
    gradW2 = dW2
    gradb1 = db1
    gradb2 = db2
    ### Stack gradients (do not modify)
    grad = np.concatenate((gradW1.flatten(), gradb1.flatten(), 
        gradW2.flatten(), gradb2.flatten()))
    
    return cost, grad
def forward_backward_prop_v1(data, labels, params, dimensions):
    """ 
    Forward and backward propagation for a two-layer sigmoidal network 
    
    Compute the forward propagation and for the cross entropy cost,
    and backward propagation for the gradients for all parameters.
    """

    ### Unpack network parameters (do not modify)
    ofs = 0
    Dx, H, Dy = (dimensions[0], dimensions[1], dimensions[2])
    assert data.shape[1] == Dx
    
    X = data.astype(float) # \in R^{N x Dx}
    labels = labels.astype(float)
    N, Dx = X.shape
    
    assert labels.shape == (N, Dy)
    W1 = np.reshape(params[ofs:ofs+ Dx * H], (Dx, H))
    ofs += Dx * H
    b1 = np.reshape(params[ofs:ofs + H], (1, H))
    ofs += H
    W2 = np.reshape(params[ofs:ofs + H * Dy], (H, Dy))
    ofs += H * Dy
    b2 = np.reshape(params[ofs:ofs + Dy], (1, Dy))
    assert b1[0].shape == (H, )
    inp1=X.dot(W1)+b1 ## R^{N x H} + (H, )
    out1=sigmoid(inp1) #(N, H)
    assert out1.shape == inp1.shape
    assert b2[0].shape == (Dy, )
    inp2=out1.dot(W2) + b2 #(N, Dy) + (Dy,)#
    assert inp2.shape == (N, Dy)
    Y_hat=softmax(inp2) # (N, Dy) #Y_hat

    loss=-np.sum(labels*np.log(Y_hat))
    cost = loss
    ### YOUR CODE HERE: forward propagation
    #raise NotImplementedError
    ### END YOUR CODE
    K=np.sum(labels, axis=1)
    K = K[np.newaxis,:].T
    ### YOUR CODE HERE: backward propagation
    dout2=(K*Y_hat-labels) #(N, Dy)
    dW2=out1.T.dot(dout2)
    assert dW2.shape == W2.shape
    db2=np.ones((1, N)).dot(dout2)
    assert db2.shape == b2.shape
    dX2=dout2.dot(W2.T)  #(N, Dy).dot((Dy,H))
    
    dout1=sigmoid_grad(out1) * dX2 # (N, H)
    assert dout1.shape == out1.shape
    dW1=X.T.dot(dout1) #(Dx, H)
    assert dW1.shape == W1.shape
    db1=np.ones((1, N)).dot(dout1) #(1, H)
    #raise NotImplementedError
    ### END YOUR CODE
    gradW1 = dW1
    gradW2 = dW2
    gradb1 = db1
    gradb2 = db2
    ### Stack gradients (do not modify)
    grad = np.concatenate((gradW1.flatten(), gradb1.flatten(), 
        gradW2.flatten(), gradb2.flatten()))
    
    return cost, grad

def sanity_check():
    """
    Set up fake data and parameters for the neural network, and test using 
    gradcheck.
    """
    print "Running sanity check..."

    N = 20
    dimensions = [10, 5, 10]
    data = np.random.randn(N, dimensions[0])   # each row will be a datum
    labels = np.zeros((N, dimensions[2]))
    for i in xrange(N):
        labels[i,random.randint(0,dimensions[2]-1)] = 1
    #labels = np.random.randn((N, dimensions[2]))
    params = np.random.randn((dimensions[0] + 1) * dimensions[1] + (
        dimensions[1] + 1) * dimensions[2], )

    gradcheck_naive(lambda params: forward_backward_prop(data, labels, params,
        dimensions), params)

def your_sanity_checks(): 
    """
    Use this space add any additional sanity checks by running:
        python q2_neural.py 
    This function will not be called by the autograder, nor will
    your additional tests be graded.
    """
    print "Running your sanity checks..."
    ### YOUR CODE HERE
    #raise NotImplementedError
    ### END YOUR CODE
    N = 20
    dimensions = [10, 5, 10]
    data = np.random.randn(N, dimensions[0])   # each row will be a datum
    
    labels = np.random.randn(N, dimensions[2])
    params = np.random.randn((dimensions[0] + 1) * dimensions[1] + (
        dimensions[1] + 1) * dimensions[2], )

    gradcheck_naive(lambda params: forward_backward_prop(data, labels, params,
        dimensions), params)
    
def main_q2_neural():
    sanity_check()
    your_sanity_checks()
    
if __name__ == "__main__":
    sanity_check()
    your_sanity_checks()