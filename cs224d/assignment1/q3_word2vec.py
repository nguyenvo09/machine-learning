import numpy as np
import random
import time
from q1_softmax import softmax
from q2_gradcheck import gradcheck_naive
from q2_sigmoid import sigmoid, sigmoid_grad




def normalizeRows(x):
    """ Row normalization function """
    # Implement a function that normalizes each row of a matrix to have unit length
    assert len(x.shape) == 1 or len(x.shape)==2, 'It should be only in 1D or 2D: %s' % (len(x.shape))
    x=x.astype(float)
    if len(x.shape) == 1:
        denom=np.sqrt(np.sum(x*x))
        x = x/(denom + 1e-9)
        return x
    elif len(x.shape) == 2:
        a=np.sqrt(np.sum(x*x, axis=1))
        a=a[np.newaxis,:].T
        x = x/(a+1e-9)
        return x
    ### YOUR CODE HERE
    #raise NotImplementedError
    ### END YOUR CODE
    
    return x

def test_normalize_rows():
    print "Testing normalizeRows..."
    x = normalizeRows(np.array([[3,4],[1, 2], [6,8], [7, 9]])) 
    # the result should be [[0.6, 0.8], [0.4472, 0.8944]]
    print x
    assert (x.all() == np.array([[0.6, 0.8], [0.4472, 0.8944], [0.6, 0.8], [0.61394061351, 0.78935221737]]).all())
    print ""

def getProbCorpus(U, vc):
    '''
    Compute probability of all words in corpus by using softmax. 
    U \in (D, W) 
    vc \in (D, )
    return (W,)
    '''
    #vc=vc[np.newaxis,:].T
    #print vc.shape
    U=U.astype(float)
    vc=vc.astype(float)
    assert len(U.shape) == 2
    D, W = U.shape
    assert U.shape[0] == vc.shape[0]
    t1=U.T.dot(vc[np.newaxis,:].T) #(W,1)
    #print t1.shape, 'here'
    t2=np.exp(t1)
    yhat=t2/(np.sum(t2) + 1e-9)
    return yhat.T[0]
    #return yhat[0]
    
def crossEntropyWord2VecVc(U, vc, y):
    """
    U \in (D, W)
    vc \in (D, )
    y \in (W, )
    return: gradient based on vc vector and U matrix.
    """
    y_hat=getProbCorpus(U, vc)
    vc=vc[np.newaxis,:].T
    loss = None
    grads={}
    loss=-np.sum(y*np.log(y_hat))
    assert y.shape == y_hat.shape, 'Mismatched shapes: %s %s' % (y.shape, y_hat.shape)
    assert y.shape[0] == U.shape[1]
    T=-y+np.sum(y)*y_hat
    t=T[np.newaxis,:].T
    grads=U.dot(t)
    assert grads.shape == vc.shape
    return loss, grads

def crossEntropyWord2VecU(U, vc, y):
    """
    U \in (D, W)
    vc \in (D, )
    y \in (W, )
    return: gradient based on U matrix. 
    """
    
    y_hat=getProbCorpus(U, vc)
    vc=vc[np.newaxis,:].T
    loss = None
    grads=None
    loss=-np.sum(y*np.log(y_hat))
    assert y.shape == y_hat.shape, 'Mismatched shapes: %s %s' % (y.shape, y_hat.shape)
    assert y.shape[0] == U.shape[1]
    t=-y+np.sum(y)*y_hat
    t=t[np.newaxis,:]
    grads=vc.dot(t)
    assert grads.shape == U.shape
    return loss, grads

def crossEntropyWord2Vec(U, vc, y):
    '''
    Compute loss and gradients based on U and vc
    U \in (D, W)
    vc \in (D, )
    y \in (W, )
    '''
    y_hat=getProbCorpus(U, vc)
    vc=vc[np.newaxis,:].T
    loss = None
    grads={}
    loss=-np.sum(y*np.log(y_hat))
    assert y.shape == y_hat.shape, 'Mismatched shapes: %s %s' % (y.shape, y_hat.shape)
    assert y.shape[0] == U.shape[1]
    T=-y+np.sum(y)*y_hat
    t=T[np.newaxis,:].T
    grads['vc']=U.dot(t)
    t=T[np.newaxis,:]
    #grads=vc.dot(t)
    grads['U']=vc.dot(t)
    #assert grads.shape == vc.shape
    return loss, grads

def singleWordCostandGradientNegativeSampling(U, vc, K, uo_index):
    """Compute loss negative sampling and gradients based on vc
    U \in (D, W): W column vectors that have D dimensions. 
    vc \in (D, )
    uo \in (D, )
    columns: (K, ) contains K indexes of selected columns.
    """
    D, W = U.shape
    columns=[]
    #assert K<W, 'The number selected columns is out of bound: needed:%s cols:%s' % (K, W)
    '''
    while(len(columns) != K):
        t=random.randint(0, W-1)
        if t == uo_index:
            continue
        columns.append(t)
    #columns=np.array(columns)
    '''
    
    
    for k in xrange(K):
        t=random.randint(0, 4)
        while t == uo_index:
            t=random.randint(0, 4)
        columns.append(t)
    
    loss=None
    grads={}
    ############################################################
    # Your Implementation is here.
    #
    ############################################################
    signs=np.array([1] + [-1 for i in xrange(K)]) #(always use numpy array, please!!!, running time changed)
    #vc_vec=vc[np.newaxis,:].T
    cols=np.array([uo_index] + columns)
    S=U[:, cols]*signs #(D, K+1)
    M=S.T.dot(vc) #(K+1)
    #M=M.flatten()
    m=sigmoid(M).flatten()
    loss = -np.sum(np.log(m))
    
    #m=sigmoid(M)-1 #(K+1, )
    dvc=S.dot(m-1)#S.dot(m[np.newaxis,:].T)
    grads['vc'] = dvc.flatten()

    dU = np.zeros(U.shape)
    #dU=dU.T

    #B=temp2*temp1
    #m=(m-1)*signs
    B=vc.reshape(D,1).dot(((m-1)*signs).reshape(1,K+1))
    #B=B*signs
    assert B.shape == (D, K+1)
    #B=B.T
    for i in xrange(K+1):
        #col=cols[i]
        dU[:, cols[i]] += B[:, i]
        #dU[col] += B[i]
    #dU=dU.T
    grads['U']=dU
    
    ############################################################
    # End of MY CODE.
    #
    ############################################################
    return loss, grads



        
def softmaxCostAndGradient_old(predicted, target, outputVectors, dataset):
    """ Softmax cost function for word2vec models """
    
    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, assuming the softmax prediction function and cross      
    # entropy loss.                                                   
    
    # Inputs:                                                         
    # - predicted: numpy ndarray, predicted word vector (\hat{v} in  
    #   the written component or \hat{r} in an earlier version)  (vc vector)
    # - target: integer, the index of the target word             
    # - outputVectors: "output" vectors (as rows) for all tokens. This matrix is U.T in pdf file.
    # - dataset: needed for negative sampling, unused here.         
    
    # Outputs:                                                        
    # - cost: cross entropy cost for the softmax word prediction    
    # - gradPred: the gradient with respect to the predicted word   
    #        vector                                                
    # - grad: the gradient with respect to all the other word        
    #        vectors                                               
    
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!       
    W,D=outputVectors.shape #(W,D)
    assert predicted.shape==(D,) #(D,)
    M=np.sum(outputVectors*predicted, axis=1)
    assert M.shape == (W,)
    M=np.exp(M)
    y_hat=M/(np.sum(M)+1e-9) #(W, )
    y=np.zeros(W)
    y[target]=1.0
    #assert target < len(y_hat), 'Index out of bound'
    #y_hat_target=y_hat[target]
    cost=-np.sum(y*np.log(y_hat))
    #cost=None
    gradPred=None
    
    T=-y+np.sum(y)*y_hat #(W,)
    t=T[np.newaxis,:].T
    gradPred=outputVectors.T.dot(t) #(D, 1)
    gradPred=gradPred.T[0]
    
    grad=predicted[np.newaxis,:].T.dot(t.T)
    grad=grad.T
    assert grad.shape == outputVectors.shape
    assert gradPred.shape == predicted.shape
    ### YOUR CODE HERE
    
    #raise NotImplementedError
    ### END YOUR CODE
    
    return cost, gradPred, grad

def negSamplingCostAndGradient_old_1(predicted, target, outputVectors, dataset, K=10):
    """ Negative sampling cost function for word2vec models """

    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, using the negative sampling technique. K is the sample  
    # size. You might want to use dataset.sampleTokenIdx() to sample  
    # a random word index. 
    # 
    # Note: See test_word2vec below for dataset's initialization.
    #                                       
    # Input/Output Specifications: same as softmaxCostAndGradient     
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ### YOUR CODE HERE
    #raise NotImplementedError
    W, D = outputVectors.shape

    loss, grads=singleWordCostandGradientNegativeSampling(outputVectors.T, predicted, K, target)
    cost = loss
    
    grad=grads['U']
    grad=grad.T
    assert grad.shape == outputVectors.shape
    
    gradPred=grads['vc']
    assert gradPred.shape == predicted.shape
    ### END YOUR CODE
    
    return cost, gradPred, grad
    
    
    
def negSamplingCostAndGradient_old(predicted, target, outputVectors, dataset, K=10):
    """ Negative sampling cost function for word2vec models """

    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, using the negative sampling technique. K is the sample  
    # size. You might want to use dataset.sampleTokenIdx() to sample  
    # a random word index. 
    # 
    # Note: See test_word2vec below for dataset's initialization.
    #                                       
    # Input/Output Specifications: same as softmaxCostAndGradient     
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ### YOUR CODE HERE
    #raise NotImplementedError
    #U = outputVectors.T
    vc=predicted
    W,D = outputVectors.shape
    columns=[]
    
    
    
    for k in xrange(K):
        t=random.randint(0, 4)
        while t == target:
            t=random.randint(0, 4)
        columns.append(t)
    
    loss=None
    grads={}
    ############################################################
    # Your Implementation is here.
    #
    ############################################################
    signs=np.array([1] + [-1 for i in xrange(K)]) #(always use numpy array, please!!!, running time changed)
    #vc_vec=vc[np.newaxis,:].T
    cols=np.array([target] + columns)
    #S=U[:, cols]*signs #(D, K+1)
    S=outputVectors[cols, :] #(K+1, D)
    M=S.dot(predicted).ravel() #(K+1)
   
    m=sigmoid(M*signs)
    cost = -np.sum(np.log(m))
    
    #m=sigmoid(M)-1 #(K+1, )
    delta=(m-1)*signs #(K+1,)
    gradPred=delta.dot(S).ravel()#S.dot(m[np.newaxis,:].T)
    #gradPred = gradPred.flatten()

    grad = np.zeros(outputVectors.shape)
    
    B=delta.reshape(K+1,1).dot(predicted.reshape(1,D))
    assert B.shape == (K+1, D)
    for i in xrange(K+1):
        grad[cols[i],:] += B[i, :]
        
    #grad=grad.T
    ### END YOUR CODE
    
    return cost, gradPred, grad    
def softmaxCostAndGradient(predicted, target, outputVectors, dataset):
    """ Softmax cost function for word2vec models """
    
    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, assuming the softmax prediction function and cross      
    # entropy loss.                                                   
    
    # Inputs:                                                         
    # - predicted: numpy ndarray, predicted word vector (\hat{v} in 
    #   the written component or \hat{r} in an earlier version)
    # - target: integer, the index of the target word               
    # - outputVectors: "output" vectors (as rows) for all tokens     
    # - dataset: needed for negative sampling, unused here.         
    
    # Outputs:                                                        
    # - cost: cross entropy cost for the softmax word prediction    
    # - gradPred: the gradient with respect to the predicted word   
    #        vector                                                
    # - grad: the gradient with respect to all the other word        
    #        vectors                                               
    
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!                                                  
    
    ### YOUR CODE HERE
    
    probabilities = softmax(predicted.dot(outputVectors.T))
    cost = -np.log(probabilities[target])
    delta = probabilities
    delta[target] -= 1
    N = delta.shape[0]
    D = predicted.shape[0]
    grad = delta.reshape((N,1)) * predicted.reshape((1,D))
    gradPred = (delta.reshape((1,N)).dot(outputVectors)).flatten()
    
    ### END YOUR CODE
    
    return cost, gradPred, grad    
def negSamplingCostAndGradient(predicted, target, outputVectors, dataset, 
    K=10):
    """ Negative sampling cost function for word2vec models """

    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, using the negative sampling technique. K is the sample  
    # size. You might want to use dataset.sampleTokenIdx() to sample  
    # a random word index. 
    # 
    # Note: See test_word2vec below for dataset's initialization.
    #                                       
    # Input/Output Specifications: same as softmaxCostAndGradient     
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ### YOUR CODE HERE
    
    grad = np.zeros(outputVectors.shape)
    gradPred = np.zeros(predicted.shape)
    
    indices = [target]
    for k in xrange(K):
        newidx = dataset.sampleTokenIdx()
        while newidx == target:
            newidx = dataset.sampleTokenIdx()
        indices += [newidx]
        
    labels = np.array([1] + [-1 for k in xrange(K)])
    vecs = outputVectors[indices,:]
    
    t = sigmoid(vecs.dot(predicted) * labels)
    cost = -np.sum(np.log(t))
    
    delta = labels * (t - 1)
    gradPred = delta.reshape((1,K+1)).dot(vecs).flatten()
    gradtemp = delta.reshape((K+1,1)).dot(predicted.reshape(
        (1,predicted.shape[0])))
    for k in xrange(K+1):
        grad[indices[k]] += gradtemp[k,:]
    
#     t = sigmoid(predicted.dot(outputVectors[target,:]))
#     cost = -np.log(t)
#     delta = t - 1

#     gradPred += delta * outputVectors[target, :]
#     grad[target, :] += delta * predicted
    
#     for k in xrange(K):
#         idx = dataset.sampleTokenIdx()
        
#         t = sigmoid(-predicted.dot(outputVectors[idx,:]))
#         cost += -np.log(t)
#         delta = 1 - t

#         gradPred += delta * outputVectors[idx, :]
#         grad[idx, :] += delta * predicted
    
    ### END YOUR CODE
    
    return cost, gradPred, grad
    
def skipgram(currentWord, C, contextWords, tokens, inputVectors, outputVectors, 
    dataset, word2vecCostAndGradient = softmaxCostAndGradient):
    """ Skip-gram model in word2vec. Fully vectorized."""

    # Implement the skip-gram model in this function.

    # Inputs:                                                         
    # - currentWord: a string of the current center word           
    # - C: integer, context size                                    
    # - contextWords: list of no more than 2*C strings, the context words                                               
    # - tokens: a dictionary that maps words to their indices in    
    #      the word vector list                                
    # - inputVectors: "input" word vectors (as rows) for all tokens           
    # - outputVectors: "output" word vectors (as rows) for all tokens         
    # - word2vecCostAndGradient: the cost and gradient function for 
    #      a prediction vector given the target word vectors,  
    #      could be one of the two cost functions you          
    #      implemented above

    # Outputs:                                                        
    # - cost: the cost function value for the skip-gram model       
    # - grad: the gradient with respect to the word vectors         
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ####################################################################
    #This implementation of this skip-gram model is not optimized.     #
    #The most optimized version is no for loop at all.        #
    ####################################################################
    assert currentWord in tokens
    idxVc=tokens[currentWord]
    vc = inputVectors[idxVc, :]
    idxsAround=[tokens[c] for c in contextWords]
    gradIn = np.zeros(inputVectors.shape)
    gradOut = np.zeros(outputVectors.shape)
    cost=0
    for idx in idxsAround:
        partialCost, gradVc, gradU=word2vecCostAndGradient(vc, idx, outputVectors, dataset)
        cost+=partialCost
        assert gradIn.shape == inputVectors.shape, 'Mismatched shape: %s %s' % (gradIn.shape, gradVc.shape)
        gradIn[idxVc,:] += gradVc
        assert gradOut.shape == gradU.shape, 'Mismatched shape: %s %s' % (gradOut.shape, gradU.shape)
        gradOut += gradU
    
    #if word2vecCostAndGradient == negSamplingCostAndGradient:
        #TODO
        
    ### YOUR CODE HERE
    #raise NotImplementedError
    ### END YOUR CODE
    
    return cost, gradIn, gradOut    

def skipgram2(currentWord, C, contextWords, tokens, inputVectors, outputVectors, 
    dataset, word2vecCostAndGradient = softmaxCostAndGradient):
    """ Skip-gram model in word2vec.  Below implementation is a correct one but quite slow due to the for loop below. """

    # Implement the skip-gram model in this function.

    # Inputs:                                                         
    # - currentWord: a string of the current center word           
    # - C: integer, context size                                    
    # - contextWords: list of no more than 2*C strings, the context words                                               
    # - tokens: a dictionary that maps words to their indices in    
    #      the word vector list                                
    # - inputVectors: "input" word vectors (as rows) for all tokens           
    # - outputVectors: "output" word vectors (as rows) for all tokens         
    # - word2vecCostAndGradient: the cost and gradient function for 
    #      a prediction vector given the target word vectors,  
    #      could be one of the two cost functions you          
    #      implemented above

    # Outputs:                                                        
    # - cost: the cost function value for the skip-gram model       
    # - grad: the gradient with respect to the word vectors         
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ####################################################################
    #This implementation of this skip-gram model is not optimized.     #
    #The most optimized version is their is no for loop at all.        #
    ####################################################################
    assert currentWord in tokens
    idxVc=tokens[currentWord]
    vc = VECTORS_IDX_VC_
    idxsAround=[tokens[c] for c in contextWords]
    gradIn = np.zeros(inputVectors.shape)
    gradOut = np.zeros(outputVectors.shape)
    cost=0
    for idx in idxsAround:
        partialCost, gradVc, gradU=word2vecCostAndGradient(vc, idx, outputVectors, dataset)
        cost+=partialCost
        assert gradIn.shape == inputVectors.shape, 'Mismatched shape: %s %s' % (gradIn.shape, gradVc.shape)
        gradIn[idxVc,:] += gradVc
        assert gradOut.shape == gradU.shape, 'Mismatched shape: %s %s' % (gradOut.shape, gradU.shape)
        gradOut += gradU
    
    
    #if word2vecCostAndGradient == softmaxCostAndGradient:
        
    #elif word2vecCostAndGradient == negSamplingCostAndGradient:
    
    #word2vecCostAndGradient(predicted, target, outputVectors, dataset)
    ### YOUR CODE HERE
    #raise NotImplementedError
    ### END YOUR CODE
    
    return cost, gradIn, gradOut

def cbow(currentWord, C, contextWords, tokens, inputVectors, outputVectors, 
    dataset, word2vecCostAndGradient = softmaxCostAndGradient):
    """ CBOW model in word2vec """

    # Implement the continuous bag-of-words model in this function.            
    # Input/Output specifications: same as the skip-gram model        
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!

    #################################################################
    # IMPLEMENTING CBOW IS EXTRA CREDIT, DERIVATIONS IN THE WRIITEN #
    # ASSIGNMENT ARE NOT!                                           #  
    #################################################################
    
    cost = 0
    gradIn = np.zeros(inputVectors.shape)
    N, D = inputVectors.shape
    gradOut = np.zeros(outputVectors.shape)
    idxsAround=[tokens[c] for c in contextWords]
    v_hat=np.zeros((D,))
    for idx in idxsAround:
        v_hat+=inputVectors[idx, :]
    idxVc=tokens[currentWord]
    #w_c=outputVectors[idxVc]
    cost, gradVc, gradOut=word2vecCostAndGradient(v_hat, idxVc, outputVectors, dataset)
    assert gradVc.shape == (D,)
    for idx in idxsAround:
        gradIn[idx,:] += gradVc
    ### YOUR CODE HERE
    #raise NotImplementedError
    ### END YOUR CODE
    
    return cost, gradIn, gradOut

#############################################
# Testing functions below. DO NOT MODIFY!   #
#############################################

def word2vec_sgd_wrapper(word2vecModel, tokens, wordVectors, dataset, C, word2vecCostAndGradient = softmaxCostAndGradient):
    batchsize = 50
    cost = 0.0
    grad = np.zeros(wordVectors.shape)
    N = wordVectors.shape[0]
    inputVectors = wordVectors[:N/2,:]
    outputVectors = wordVectors[N/2:,:]
    for i in xrange(batchsize):
        C1 = random.randint(1,C)
        centerword, context = dataset.getRandomContext(C1)
        
        if word2vecModel == skipgram:
            denom = 1
        else:
            denom = 1
        #tic=time.time()
        c, gin, gout = word2vecModel(centerword, C1, context, tokens, inputVectors, outputVectors, dataset, word2vecCostAndGradient)
        #toc=time.time()
        #print 'Running time of this loop: %s' % (toc-tic)
        cost += c / batchsize / denom
        grad[:N/2, :] += gin / batchsize / denom
        assert gout.shape == outputVectors.shape
        grad[N/2:, :] += gout / batchsize / denom
        
    return cost, grad

def test_word2vec():
    # Interface to the dataset for negative sampling
    
    dataset = type('dummy', (), {})()
    def dummySampleTokenIdx():
        return random.randint(0, 4)

    def getRandomContext(C):
        tokens = ["a", "b", "c", "d", "e"]
        return tokens[random.randint(0,4)], [tokens[random.randint(0,4)] \
           for i in xrange(2*C)]
    dataset.sampleTokenIdx = dummySampleTokenIdx
    dataset.getRandomContext = getRandomContext

    random.seed(31415)
    np.random.seed(9265)
    dummy_vectors = normalizeRows(np.random.randn(10,3))
    dummy_tokens = dict([("a",0), ("b",1), ("c",2),("d",3),("e",4)])
    print "==== Gradient check for skip-gram ===="
    tic=time.time()
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5), dummy_vectors)
    toc=time.time()
    print 'Running time of softmaxCostAndGradient: %s' % (toc-tic)
    tic=time.time()
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient), dummy_vectors)
    toc=time.time()
    print 'Running time of negSamplingCostAndGradient: %s' % (toc-tic)
    tic=time.time()
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient_old), dummy_vectors)
    toc=time.time()
    print 'Running time of negSamplingCostAndGradient_old My implementation: %s' % (toc-tic)

    tic=time.time()
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient_old_1), dummy_vectors)
    toc=time.time()
    print 'Running time of negSamplingCostAndGradient_old_2 My implementation: %s' % (toc-tic)
    print "\n==== Gradient check for CBOW      ===="
    tic=time.time()
    #gradcheck_naive(lambda vec: word2vec_sgd_wrapper(cbow, dummy_tokens, vec, dataset, 5), dummy_vectors)
    #gradcheck_naive(lambda vec: word2vec_sgd_wrapper(cbow, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient), dummy_vectors)
    toc=time.time()
    print 'Running time of cbow of two method: %s' % (toc-tic)
    return
    print "\n=== Results ==="
    print skipgram("c", 3, ["a", "b", "e", "d", "b", "c"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset)
    print skipgram("c", 1, ["a", "b"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset, negSamplingCostAndGradient)
    print cbow("a", 2, ["a", "b", "c", "a"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset)
    print cbow("a", 2, ["a", "b", "a", "c"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset, negSamplingCostAndGradient)

if __name__ == "__main__":
    test_normalize_rows()
    test_word2vec()