import numpy as np
import random
from random import randrange

# First implement a gradient checker by filling in the following functions
def gradcheck_naive(f, x):
    """ 
    Gradient check for a function f 
    - f should be a function that takes a single argument and outputs the cost and its gradients
    - x is the point (numpy array) to check the gradient at
    """ 

    rndstate = random.getstate()
    random.setstate(rndstate)  
    fx, grad = f(x) # Evaluate function value at original point
    random.setstate(rndstate)
    h = 1e-5

    # Iterate over all indexes in x
    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        ix = it.multi_index
        
        ### try modifying x[ix] with h defined above to compute numerical gradients
        ### make sure you call random.setstate(rndstate) before calling f(x) each time, this will make it 
        ### possible to test cost functions with built in randomness later
        ### YOUR CODE HERE:
        old_x=x
        raw=x[ix]
        x[ix]=raw+h
        random.setstate(rndstate)
        oldval,_=f(x)
        random.setstate(rndstate)
        x[ix]=raw-h
        newval,_=f(x)
        random.setstate(rndstate)
        x[ix]=raw
        assert np.sum(abs(x-old_x)) <= 1e-5, 'x vector is changing %s - %s' % (old_x, x)
        numgrad = (oldval-newval)/ (2*h)
        #raise NotImplementedError
        ### END YOUR CODE

        # Compare gradients
        reldiff = abs(numgrad - grad[ix]) / max(1, abs(numgrad), abs(grad[ix]))
        if reldiff > 1e-5:
            print "Gradient check failed."
            print "First gradient error found at index %s" % str(ix)
            print "Your gradient: %f \t Numerical gradient: %f" % (grad[ix], numgrad)
            return
    
        it.iternext() # Step to next dimension

    print "Gradient check passed!"
def my_gradcheck_naive(f, x):
    """ 
    Gradient check for a function f 
    - f should be a function that takes a single argument and outputs the cost and its gradients
    - x is the point (numpy array) to check the gradient at
    """ 

    rndstate = random.getstate()
    random.setstate(rndstate)  
    fx, grad = f(x) # Evaluate function value at original point
    h = 1e-5

    # Iterate over all indexes in x
    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    old_x=x
    while not it.finished:
        ix = it.multi_index
        
        ### try modifying x[ix] with h defined above to compute numerical gradients
        ### make sure you call random.setstate(rndstate) before calling f(x) each time, this will make it 
        ### possible to test cost functions with built in randomness later
        ### YOUR CODE HERE:
        
        raw=x[ix]
        x[ix]=raw+h
        temp1=np.array(x, copy=True)
        random.setstate(rndstate)
        oldval,_=f(x)
        random.setstate(rndstate)
        x[ix]=raw-h
        temp2=np.array(x, copy=True)
        newval,_=f(x)
        x[ix]=raw
        assert np.sum(abs(x-old_x)) <= 1e-5, 'x vector is changing %s - %s' % (old_x, x)
        numgrad = (oldval-newval)/ (2*h)
        #raise NotImplementedError
        ### END YOUR CODE

        # Compare gradients
        reldiff = abs(numgrad - grad[ix]) / max(1, abs(numgrad), abs(grad[ix]))
        print "Your gradient: %f \t Numerical gradient: %f \t diffence:%s" % (grad[ix], numgrad, reldiff)
        if reldiff > 1e-5:
            print "======================Gradient check failed.=========================="
            print 'x[ix]+h: ', temp1, f(temp1)[0]
            print 'x[ix]-h: ', temp2, f(temp2)[0]
            k=f(temp1)[0] - f(temp2)[0]
            print k, 2*h
            print 'Gia tri gradient la:', k/(2*h)
            print "First gradient error found at index %s" % str(ix)
            #print "Your gradient: %f \t Numerical gradient: %f" % (grad[ix], numgrad)\
            print "======================================================================"
        #   return
    
        it.iternext() # Step to next dimension

    print "Gradient check passed!"

def grad_check_sparse(f, x, analytic_grad, num_checks=10, h=1e-5):
  """
  sample a few random elements and only return numerical
  in this dimensions.
  """

  for i in xrange(num_checks):
    ix = tuple([randrange(m) for m in x.shape])

    oldval = x[ix]
    x[ix] = oldval + h # increment by h
    fxph = f(x) # evaluate f(x + h)
    x[ix] = oldval - h # increment by h
    fxmh = f(x) # evaluate f(x - h)
    x[ix] = oldval # reset

    grad_numerical = (fxph - fxmh) / (2 * h)
    grad_analytic = analytic_grad[ix]
    rel_error = abs(grad_numerical - grad_analytic) / (abs(grad_numerical) + abs(grad_analytic))
    print 'numerical: %f analytic: %f, relative error: %f' % (grad_numerical, grad_analytic, rel_error)
    

def grad_check_full(f, x, analytic_grad, num_checks=10, h=1e-5):
  """
  full random elements and only return numerical
  in this dimensions.
  """
  
  rndstate = random.getstate()
  random.setstate(rndstate)  
  it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
  while not it.finished:
    ix = it.multi_index

    oldval = x[ix]
    x[ix] = oldval + h # increment by h
    random.setstate(rndstate)
    fxph = f(x) # evaluate f(x + h)
    x[ix] = oldval - h # increment by h
    random.setstate(rndstate)
    fxmh = f(x) # evaluate f(x - h)
    x[ix] = oldval # reset
    random.setstate(rndstate)
    grad_numerical = (fxph - fxmh) / (2 * h)
    grad_analytic = analytic_grad[ix]
    rel_error = abs(grad_numerical - grad_analytic) / (abs(grad_numerical) + abs(grad_analytic))
    print 'Position: %s numerical: %f analytic: %f, relative error: %f' % (ix, grad_numerical, grad_analytic, rel_error)
    it.iternext() # Step to next dimension
    
def sanity_check():
    """
    Some basic sanity checks.
    """
    quad = lambda x: (np.sum(x ** 2), x * 2)

    print "Running sanity checks..."
    gradcheck_naive(quad, np.array(123.456))      # scalar test
    gradcheck_naive(quad, np.random.randn(3,))    # 1-D test
    gradcheck_naive(quad, np.random.randn(4,5))   # 2-D test
    print ""

def your_sanity_checks(): 
    """
    Use this space add any additional sanity checks by running:
        python q2_gradcheck.py 
    This function will not be called by the autograder, nor will
    your additional tests be graded.
    """
    print "Running your sanity checks..."
    ### YOUR CODE HERE
    raise NotImplementedError
    ### END YOUR CODE
def main_q2():
    sanity_check()
if __name__ == "__main__":
    sanity_check()
    #your_sanity_checks()
