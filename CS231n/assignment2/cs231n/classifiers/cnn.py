import numpy as np

from cs231n.layers import *
from cs231n.fast_layers import *
from cs231n.layer_utils import *


class ThreeLayerConvNet(object):
  """
  A three-layer convolutional network with the following architecture:
  
  conv - relu - 2x2 max pool - affine - relu - affine - softmax
  @ben: (2x2) max pool in this case means that: The convolutional layer will be cropped half (e.g., stride of max pool is 2)
  @ben: conv-relu: means that each filter of conv will be squished down by relu. ex: conv: (X, Y, Z) => relu => (X, Y, Z)
  @ben: This structure can be explained: input_dim - (conv - relu - 2x2 max pool) - (affine - relue) - (affine - softmax)
  
  The network operates on minibatches of data that have shape (N, C, H, W)
  consisting of N images, each with height H and width W and with C input
  channels.
  """
  
  def __init__(self, input_dim=(3, 32, 32), num_filters=32, filter_size=7,
               hidden_dim=100, num_classes=10, weight_scale=1e-3, reg=0.0,
               dtype=np.float32):
    """
    Initialize a new network.
    
    Inputs:
    - input_dim: Tuple (C, H, W) giving size of input data
    - num_filters: Number of filters to use in the convolutional layer
    - filter_size: Size of filters to use in the convolutional layer
    - hidden_dim: Number of units to use in the fully-connected hidden layer
    - num_classes: Number of scores to produce from the final affine layer.
    - weight_scale: Scalar giving standard deviation for random initialization
      of weights.
    - reg: Scalar giving L2 regularization strength
    - dtype: numpy datatype to use for computation.
    """
    self.params = {}
    self.reg = reg
    self.dtype = dtype
    self.num_classes = num_classes
    self.hidden_dim = hidden_dim
    self.input_dim = input_dim
    self.chanels=input_dim[0]
    self.num_filters = num_filters
    self.filter_size = filter_size
    self.max_pool_window_size=2
    self.max_pool_stride=2
    self.pad=0
    conv_param = {'stride': 1, 'pad': (filter_size - 1) / 2}
    self.conv_param = conv_param
    ############################################################################
    # TODO: Initialize weights and biases for the three-layer convolutional    #
    # network. Weights should be initialized from a Gaussian with standard     #
    # deviation equal to weight_scale; biases should be initialized to zero.   #
    # All weights and biases should be stored in the dictionary self.params.   #
    # Store weights and biases for the convolutional layer using the keys 'W1' #
    # and 'b1'; use keys 'W2' and 'b2' for the weights and biases of the       #
    # hidden affine layer, and keys 'W3' and 'b3' for the weights and biases   #
    # of the output affine layer.                                              #
    ############################################################################
    #pass
    C, inpH, inpW = input_dim
    b1 = np.zeros(num_filters) #Each filter has one bias number
    W1 = weight_scale*np.random.randn(num_filters, C, filter_size, filter_size) #Each filter has a windows of size "filter_size" => params: filter_size*filter_size*C
    
    pad = self.conv_param['pad']
    stride_conv=self.conv_param['stride']
    #suppose that our window size is (filter_size, filter_size, C)
    assert (inpH +2*pad-filter_size) % (stride_conv) == 0
    assert (inpW + 2*pad - filter_size) % stride_conv == 0
    conv_H=1+(inpH +2*pad-filter_size)/stride_conv
    conv_W=1+(inpW +2*pad-filter_size)/stride_conv
    conv_depth=num_filters
    
    pool_param = {'pool_height': 2, 'pool_width': 2, 'stride': 2}
    self.pool_param = pool_param
    assert conv_H%self.pool_param['pool_height'] == 0 
    assert conv_W%self.pool_param['pool_width'] == 0
    #size after max-pool with windowsize (2,2)
    conv_H_cropped=conv_H/2
    conv_W_cropped=conv_W/2
    
    b2=np.zeros(hidden_dim)
    W2 = weight_scale*np.random.randn(conv_H_cropped*conv_W_cropped*conv_depth, hidden_dim)
    #after max-pooling, we flatten pooled-output to an 1D array with size conv_H_cropped*conv_W_cropped*conv_depth
    self.output_conv_relu_pooled_H=conv_H_cropped
    self.output_conv_relu_pooled_W=conv_W_cropped
    self.output_conv_relu_pooled=conv_depth
    
    b3=np.zeros(num_classes)
    W3=weight_scale*np.random.randn(hidden_dim, num_classes)
    
    self.params['b1'] = b1
    self.params['W1'] = W1
    self.params['b2'] = b2
    self.params['W2'] = W2
    self.params['b3'] = b3
    self.params['W3'] = W3
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    for k, v in self.params.iteritems():
      self.params[k] = v.astype(dtype)
     
 
  def loss(self, X, y=None):
    """
    Evaluate loss and gradient for the three-layer convolutional network.
    
    Input / output: Same API as TwoLayerNet in fc_net.py.
    """
    W1, b1 = self.params['W1'], self.params['b1']
    W2, b2 = self.params['W2'], self.params['b2']
    W3, b3 = self.params['W3'], self.params['b3']
    
    # pass conv_param to the forward pass for the convolutional layer
    filter_size = W1.shape[2]
    assert W1.shape == (self.num_filters, self.chanels, self.filter_size, self.filter_size)
    conv_param = {'stride': 1, 'pad': (filter_size - 1) / 2}

    # pass pool_param to the forward pass for the max-pooling layer
    pool_param = {'pool_height': 2, 'pool_width': 2, 'stride': 2}
    assert len(X.shape) == 4 #N, C, H, W
    N,C,H,W=X.shape
    assert (C,H,W) == self.input_dim
    scores = None
    ############################################################################
    # TODO: Implement the forward pass for the three-layer convolutional net,  #
    # computing the class scores for X and storing them in the scores          #
    # variable.                                                                #
    ############################################################################
    #pass
    out_conv_pooled, cache = conv_relu_pool_forward(X, W1, b1, conv_param, pool_param)
    assert out_conv_pooled.shape == (N, self.num_filters, self.output_conv_relu_pooled_H, self.output_conv_relu_pooled_W)
    
    
    out_lay2, cache_lay2 = affine_relu_forward(out_conv_pooled, W2, b2)
    assert out_lay2.shape == (N, self.hidden_dim)
    feed_softmax, cache_last_affine = affine_forward(out_lay2, W3, b3)
    scores = softmax_score(feed_softmax)
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################
    
    if y is None:
      return scores
    
    loss, grads = 0, {}
    ############################################################################
    # TODO: Implement the backward pass for the three-layer convolutional net, #
    # storing the loss and gradients in the loss and grads variables. Compute  #
    # data loss using softmax, and make sure that grads[k] holds the gradients #
    # for self.params[k]. Don't forget to add L2 regularization!               #
    ############################################################################
    #pass
    loss, dTop = softmax_loss(feed_softmax, y)
    loss += 0.5*self.reg*(np.sum(W1*W1) + np.sum(W2*W2) + np.sum(W3*W3))
    
    
    dX3, dW3,db3 = affine_backward(dTop, cache_last_affine)
    dW3 += self.reg*W3
    
    dX2, dW2, db2 = affine_relu_backward(dX3, cache_lay2)
    dW2 += self.reg*W2
    
    dX1, dW1, db1 = conv_relu_pool_backward(dX2, cache)
    dW1 += self.reg*W1
    assert dW1.shape == W1.shape
    
    grads['b1'] = db1
    grads['b2'] = db2
    grads['b3'] = db3
    grads['W1'] = dW1
    grads['W2'] = dW2
    grads['W3'] = dW3
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################
    
    return loss, grads
  
  
pass
