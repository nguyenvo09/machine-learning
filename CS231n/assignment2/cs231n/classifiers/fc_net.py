import numpy as np

from cs231n.layers import *
from cs231n.layer_utils import *


class TwoLayerNet(object):
  """
  A two-layer fully-connected neural network with ReLU nonlinearity and
  softmax loss that uses a modular layer design. We assume an input dimension
  of D, a hidden dimension of H, and perform classification over C classes.
  
  The architecure should be affine - relu - affine - softmax.

  Note that this class does not implement gradient descent; instead, it
  will interact with a separate Solver object that is responsible for running
  optimization.

  The learnable parameters of the model are stored in the dictionary
  self.params that maps parameter names to numpy arrays.
  """
  
  def __init__(self, input_dim=3*32*32, hidden_dim=100, num_classes=10,
               weight_scale=1e-3, reg=0.0):
    """
    Initialize a new network.

    Inputs:
    - input_dim: An integer giving the size of the input
    - hidden_dim: An integer giving the size of the hidden layer
    - num_classes: An integer giving the number of classes to classify
    - dropout: Scalar between 0 and 1 giving dropout strength.
    - weight_scale: Scalar giving the standard deviation for random
      initialization of the weights.
    - reg: Scalar giving L2 regularization strength.
    """
    self.params = {}
    self.reg = reg
    
    ############################################################################
    # TODO: Initialize the weights and biases of the two-layer net. Weights    #
    # should be initialized from a Gaussian with standard deviation equal to   #
    # weight_scale, and biases should be initialized to zero. All weights and  #
    # biases should be stored in the dictionary self.params, with first layer  #
    # weights and biases using the keys 'W1' and 'b1' and second layer weights #
    # and biases using the keys 'W2' and 'b2'.                                 #
    ############################################################################
    pass
    self.params['b1']=np.zeros(hidden_dim)
    self.params['b2']=np.zeros(num_classes)
    self.params['W1']=weight_scale * np.random.randn(input_dim, hidden_dim)
    self.params['W2']=weight_scale * np.random.randn(hidden_dim, num_classes)
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################
  def loss(self, X, y=None):
    #print ''
    """
    This function should use API in layer_utils.py
    Compute loss and gradient for a minibatch of data.

    Inputs:
    - X: Array of input data of shape (N, d_1, ..., d_k)
    - y: Array of labels, of shape (N,). y[i] gives the label for X[i].

    Returns:
    If y is None, then run a test-time forward pass of the model and return:
    - scores: Array of shape (N, C) giving classification scores, where
      scores[i, c] is the classification score for X[i] and class c.

    If y is not None, then run a training-time forward and backward pass and
    return a tuple of:
    - loss: Scalar value giving the loss
    - grads: Dictionary with the same keys as self.params, mapping parameter
      names to gradients of the loss with respect to those parameters.
    """  
    scores = None
    W1=self.params['W1'] #R^{D x hidden_dim}
    b1=self.params['b1'] #R^{hidden_dim}
    W2=self.params['W2'] #R^{hidden_dim x num_classes}
    b2=self.params['b2'] #R^{num_classes}
    input_shape=X.shape
    N=input_shape[0]
    D=np.prod(input_shape)/N
    C=b2.shape[0]
    hidden_dim=b1.shape[0]
    a1, cache1 = affine_forward(X, W1, b1)
    assert a1.shape==(N, hidden_dim)
    out1_test, _=affine_relu_forward(X,W1, b1)
    out1, cache2=relu_forward(a1)
    assert out1.shape == out1_test.shape
    assert np.abs(out1-out1_test).sum() <= 1e-10, 'Two functions have different results'
    
    a2, cache3 = affine_forward(out1, W2, b2) #a2 \in R^{N x C}
    
    #a2,_ = affine_relu_forward(X,W)
    
    scores=a2
    
    if y is None:
      return scores
    loss, grads = 0, {}
    loss_softmax, dLossBasedOnScore = softmax_loss(scores, y) 
    loss = loss_softmax + self.reg*0.5*np.sum(W1*W1) + self.reg*0.5*np.sum(W2*W2)
    
    assert dLossBasedOnScore.shape == a2.shape
    dHiddenLayerRight, dW2, db2 = affine_backward(dLossBasedOnScore, cache3)
    dW2 += self.reg*W2
    
    da1=relu_backward(dHiddenLayerRight, cache2)
    dX, dW1, db1 = affine_backward(da1, cache1)
    dW1 += self.reg * W1
    grads['W1']=dW1
    grads['W2']=dW2
    grads['b1']=db1
    grads['b2']=db2
    return loss, grads
    
    
  def loss1(self, X, y=None):
    """
    This function implements all gradient from scratch, without calling backward or forward from other stuff. 
    Too lengthy and not efficient for code maintenance 
    Compute loss and gradient for a minibatch of data.

    Inputs:
    - X: Array of input data of shape (N, d_1, ..., d_k)
    - y: Array of labels, of shape (N,). y[i] gives the label for X[i].

    Returns:
    If y is None, then run a test-time forward pass of the model and return:
    - scores: Array of shape (N, C) giving classification scores, where
      scores[i, c] is the classification score for X[i] and class c.

    If y is not None, then run a training-time forward and backward pass and
    return a tuple of:
    - loss: Scalar value giving the loss
    - grads: Dictionary with the same keys as self.params, mapping parameter
      names to gradients of the loss with respect to those parameters.
    """  
    scores = None
    W1=self.params['W1'] #R^{D x hidden_dim}
    b1=self.params['b1'] #R^{hidden_dim}
    W2=self.params['W2'] #R^{hidden_dim x num_classes}
    b2=self.params['b2'] #R^{num_classes}
    input_shape=X.shape
    N=input_shape[0]
    D=np.prod(input_shape)/N
    C=b2.shape[0]
    X_new=X.reshape(N,D)
    a1=X_new.dot(W1)+b1[np.newaxis,:]
    mask=np.where(a1>0, 1, 0)
    out1=mask*a1 #R^{N x hidden_dim}
    a2=out1.dot(W2)+b2[np.newaxis,:] #R^{N*num_classes}
    scores=a2
    
    ############################################################################
    # TODO: Implement the forward pass for the two-layer net, computing the    #
    # class scores for X and storing them in the scores variable.              #
    ############################################################################
    #pass
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # If y is None then we are in test mode so just return scores
    if y is None:
      return scores
    t1=np.exp(a2)
    t2=np.sum(t1, axis=1) + 1e-10# sum theo row
    t3=t1[np.arange(N), y]
    t3/=t2
    loss, grads = 0, {}
    
    loss = 1.0/N * np.sum(-np.log(t3)) + self.reg*0.5*np.sum(W1*W1) + self.reg*0.5*np.sum(W2*W2)

    ############################################################################
    # TODO: Implement the backward pass for the two-layer net. Store the loss  #
    # in the loss variable and gradients in the grads dictionary. Compute data #
    # loss using softmax, and make sure that grads[k] holds the gradients for  #
    # self.params[k]. Don't forget to add L2 regularization!                   #
    #                                                                          #
    # NOTE: To ensure that your implementation matches ours and you pass the   #
    # automated tests, make sure that your L2 regularization includes a factor #
    # of 0.5 to simplify the expression for the gradient.                      #
    ############################################################################
    #pass
    dLossBasedScoreVector=np.zeros((N,C))
    temp1=t1/t2[np.newaxis,:].T
    temp1[np.arange(N),y]-=1
    dLossBasedScoreVector=1.0/N*temp1 #R^{NxC}
    assert dLossBasedScoreVector.shape==a2.shape, 'Mismatched shape, gradient must matched shape'
    db2=np.ones((1,N)).dot(dLossBasedScoreVector)
    db2=db2[0]
    dW2=out1.T.dot(dLossBasedScoreVector) + self.reg*W2#R^{hidden_dim x num_classes}
    assert dW2.shape == W2.shape
    dOut1=W2.dot(dLossBasedScoreVector.T) #R^{hidden_dim x N}
    dOut1=dOut1.T
    assert dOut1.shape == out1.shape
    dW1=X_new.T.dot(dOut1) + self.reg*W1 #D x N dot N x hidden_dim
    db1=np.ones((1,N)).dot(dOut1)
    db1=db1[0]
    grads['W1']=dW1
    grads['W2']=dW2
    grads['b1']=db1
    grads['b2']=db2
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads


class FullyConnectedNet(object):
  """
  A fully-connected neural network with an arbitrary number of hidden layers,
  ReLU nonlinearities, and a softmax loss function. This will also implement
  dropout and batch normalization as options. For a network with L layers,
  the architecture will be
  
  {affine - [batch norm] - relu - [dropout]} x (L - 1) - affine - softmax
  
  where batch normalization and dropout are optional, and the {...} block is
  repeated L - 1 times.
  
  Similar to the TwoLayerNet above, learnable parameters are stored in the
  self.params dictionary and will be learned using the Solver class.
  """

  def __init__(self, hidden_dims, input_dim=3*32*32, num_classes=10,
               dropout=0, use_batchnorm=False, reg=0.0,
               weight_scale=1e-2, dtype=np.float32, seed=None):
    """
    Initialize a new FullyConnectedNet.
    
    Inputs:
    - hidden_dims: A list of integers giving the size of each hidden layer.
    - input_dim: An integer giving the size of the input.
    - num_classes: An integer giving the number of classes to classify.
    - dropout: Scalar between 0 and 1 giving dropout strength. If dropout=0 then
      the network should not use dropout at all.
    - use_batchnorm: Whether or not the network should use batch normalization.
    - reg: Scalar giving L2 regularization strength.
    - weight_scale: Scalar giving the standard deviation for random
      initialization of the weights.
    - dtype: A numpy datatype object; all computations will be performed using
      this datatype. float32 is faster but less accurate, so you should use
      float64 for numeric gradient checking.
    - seed: If not None, then pass this random seed to the dropout layers. This
      will make the dropout layers deteriminstic so we can gradient check the
      model.
    """
    self.use_batchnorm = use_batchnorm
    self.use_dropout = dropout > 0
    self.reg = reg
    self.num_layers = 1 + len(hidden_dims)
    self.num_hidden_layers=len(hidden_dims)
    self.dtype = dtype
    self.params = {}
    self.weight_scale = weight_scale
    self.num_classes = num_classes
    ############################################################################
    # TODO: Initialize the parameters of the network, storing all values in    #
    # the self.params dictionary. Store weights and biases for the first layer #
    # in W1 and b1; for the second layer use W2 and b2, etc. Weights should be #
    # initialized from a normal distribution with standard deviation equal to  #
    # weight_scale and biases should be initialized to zero.                   #
    #                                                                          #
    # When using batch normalization, store scale and shift parameters for the #
    # first layer in gamma1 and beta1; for the second layer use gamma2 and     #
    # beta2, etc. Scale parameters should be initialized to one and shift      #
    # parameters should be initialized to zero.                                #
    ############################################################################
    #pass
    first=[input_dim] + hidden_dims
    second=hidden_dims + [num_classes]
    assert len(first) == len(second)
    assert len(first) == self.num_layers
    between=zip(first, second)
    for i in range(self.num_layers):
        bet=between[i]
        assert len(bet) == 2
        self.params['b%s' % (i+1)] = np.zeros(bet[1])
        self.params['W%s' % (i+1)] = weight_scale * np.random.randn(bet[0], bet[1])
        if i != self.num_layers-1 and self.use_batchnorm:
            self.params['gamma%s' % (i+1)] = np.ones(bet[1]) #only for hidden layers!!!!!!!
            self.params['beta%s' % (i+1)] = np.zeros(bet[1])
    if self.use_batchnorm:
        assert len(self.params) == 4*(self.num_layers-1) + 2
    else:
        assert len(self.params) == 2*(self.num_layers)
    self._test_initialization()
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # When using dropout we need to pass a dropout_param dictionary to each
    # dropout layer so that the layer knows the dropout probability and the mode
    # (train / test). You can pass the same dropout_param to each dropout layer.
    self.dropout_param = {}
    if self.use_dropout:
      self.dropout_param = {'mode': 'train', 'p': dropout}
      if seed is not None:
        self.dropout_param['seed'] = seed
    
    # With batch normalization we need to keep track of running means and
    # variances, so we need to pass a special bn_param object to each batch
    # normalization layer. You should pass self.bn_params[0] to the forward pass
    # of the first batch normalization layer, self.bn_params[1] to the forward
    # pass of the second batch normalization layer, etc.
    self.bn_params = []
    if self.use_batchnorm:
      self.bn_params = [{'mode': 'train'} for i in xrange(self.num_layers - 1)]
        
    
    # Cast all parameters to the correct datatype
    for k, v in self.params.iteritems():
      self.params[k] = v.astype(dtype)
   
   
  def _test_initialization(self):
    """ Test correctness of intialization of parameters. 
    This is important since intialization plays important role in performance of neural network. 
    Don't call this function manually. 
    """
    for i in range(self.num_layers):
        #bet=between[i]
        assert 'W%s' % (i+1) in self.params
        assert 'b%s' % (i+1) in self.params
        W=self.params['W%s' % (i+1)]
        b=self.params['b%s' % (i+1)]
        assert b.shape[0] == W.shape[1]
        std=self.weight_scale
  
        W_std = abs(W.std() - std)
        
        assert W_std < std / 10, '%s layer weights do not seem right: %s' % (i+1, W_std)
        assert np.all(b == 0), '%s layer biases do not seem right' % (i+1)
        
  
  def _getL2Regularization(self, num_layers):
    ''' Compute regularization of all W matrices between layers. biases do not need to regularized. 
        Don't call this function manually
    '''
    L2_regs=0
    for i in range(num_layers):
       W=self.params['W%s' % (i+1)]
       L2_regs+=np.sum(W*W)
    return L2_regs    
    
    
  def loss(self, X, y=None):
    """
    Compute loss and gradient for the fully-connected net.

    Input / output: Same as TwoLayerNet above.
    """
    X = X.astype(self.dtype)
    input_shape=np.prod(X.shape)
    N = X.shape[0]
    D = input_shape / N
    assert input_shape % N == 0
    X_ = X.reshape(N,D)
    mode = 'test' if y is None else 'train'

    # Set train/test mode for batchnorm params and dropout param since they
    # behave differently during training and testing.
    if self.dropout_param is not None:
      self.dropout_param['mode'] = mode   
    if self.use_batchnorm:
      for bn_param in self.bn_params:
        bn_param[mode] = mode

    scores = None
    ############################################################################
    # TODO: Implement the forward pass for the fully-connected net, computing  #
    # the class scores for X and storing them in the scores variable.          #
    #                                                                          #
    # When using dropout, you'll need to pass self.dropout_param to each       #
    # dropout forward pass.                                                    #
    #                                                                          #
    # When using batch normalization, you'll need to pass self.bn_params[0] to #
    # the forward pass for the first batch normalization layer, pass           #
    # self.bn_params[1] to the forward pass for the second batch normalization #
    # layer, etc.                                                              #
    ############################################################################
    #pass
    inp_ = X_
    layer_outs={}
    cache_last = None
    for i in range(self.num_layers):
      W_ = self.params['W%s' % (i+1)]
      b_ = self.params['b%s' % (i+1)]
      if i == self.num_layers-1:
        #In the last hidden layer, to compute scores, you just apply affine transformation. The "scores" will be the input of 
        # output layer. Therefore, you only use "affine_forward"
        scores, cache_last = affine_forward(inp_, W_, b_)
      else:
        if self.use_batchnorm:
            out_, cache_ = affine_batchnorm_relu_forward(inp_, W_, b_, 
                                                        self.params['gamma%s' %(i+1)], 
                                                        self.params['beta%s' % (i+1)], 
                                                        self.bn_params[i])
        else:
            out_, cache_ = affine_relu_forward(inp_, W_, b_)
            
        if self.use_dropout:
            #print 'Using drop out forward'
            out_, cache_dropout = dropout_forward(out_, self.dropout_param)
            cache_ = (cache_, cache_dropout)
        
        layer_outs['lay%s' % (i+1)] = (out_, cache_) #Store the output of after each hidden layer: for example: lay1: [3,4,5]
        inp_ = out_
            
      
    assert len(layer_outs) == self.num_hidden_layers
    assert scores.shape == (N, self.num_classes)
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # If test mode return early
    if mode == 'test':
      return scores
    
    
    loss, grads = 0.0, {}
    
    loss_softmax, dLossBasedOnScore = softmax_loss(scores, y) 
    L2_regs=self._getL2Regularization(self.num_layers)
    loss = loss_softmax + self.reg*0.5*(L2_regs)
    
    assert dLossBasedOnScore.shape == scores.shape
    
    ############################################################################
    # TODO: Implement the backward pass for the fully-connected net. Store the #
    # loss in the loss variable and gradients in the grads dictionary. Compute #
    # data loss using softmax, and make sure that grads[k] holds the gradients #
    # for self.params[k]. Don't forget to add L2 regularization!               #
    #                                                                          #
    # When using batch normalization, you don't need to regularize the scale   #
    # and shift parameters.                                                    #
    #                                                                          #
    # NOTE: To ensure that your implementation matches ours and you pass the   #
    # automated tests, make sure that your L2 regularization includes a factor #
    # of 0.5 to simplify the expression for the gradient.                      #
    ############################################################################
    #pass
    
    doutHiddenLayer = None
    for i in range(self.num_layers-1, -1, -1):
      #Loop from self.num_layers-1 -> 0.
      W_ = self.params['W%s' % (i+1)]
      b_ = self.params['b%s' % (i+1)]
      if i == self.num_layers-1:
        doutLastHiddenLayer, dW_last, db_last = affine_backward(dLossBasedOnScore, cache_last)
        dW_last += self.reg*W_
        assert dW_last.shape == W_.shape
        assert db_last.shape == b_.shape
        grads['W%s' % (i+1)] = dW_last
        grads['b%s' % (i+1)] = db_last
        doutHiddenLayer = doutLastHiddenLayer
      else:
        assert doutHiddenLayer != None
        out_, cache_= layer_outs['lay%s' % (i+1)] 
        #If use dropout
        if self.use_dropout:
            #print 'Using drop out backward'
            cache_before_dropout, cache_dropout = cache_ 
            dDropout = dropout_backward(doutHiddenLayer, cache_dropout)
            doutHiddenLayer = dDropout
            cache_ = cache_before_dropout
            #cache_ = (cache_, cache_dropout)
        
        if self.use_batchnorm:
            dInp_, dw, db, dgamma, dbeta = affine_batchnorm_relu_backward(doutHiddenLayer, cache_)
            grads['gamma%s' % (i+1)] = dgamma
            grads['beta%s' % (i+1)] = dbeta
        else:
            dInp_, dw, db = affine_relu_backward(doutHiddenLayer, cache_)
        
        
        
        dw+= self.reg*W_
        assert dw.shape == W_.shape
        assert db.shape == b_.shape
        doutHiddenLayer = dInp_
        grads['W%s' % (i+1)] = dw
        grads['b%s' % (i+1)] = db
    
    
    
    #da1=relu_backward(dHiddenLayerRight, cache2)
    #dX, dW1, db1 = affine_backward(da1, cache1)
    #dW1 += self.reg * W1
    
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads
