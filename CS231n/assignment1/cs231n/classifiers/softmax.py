import numpy as np
from random import shuffle

def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)
  D,nclasses = W.shape
  assert X.shape[1] == D 
  nobs=X.shape[0]
  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  for i in range(nobs):
    xi=X[i]
    scores=xi.dot(W)
    T=np.exp(scores)
    prob=T[y[i]]/np.sum(T)
    loss+=-np.log(prob)
  #pass
  loss /= nobs
  loss += 0.5*reg * np.sum(W * W)
  
  #Computing gradient in looping method
  for i in range(nobs):
    xi=X[i]
    scores=xi.dot(W) # \in R^{1xC}
    T=np.exp(scores) # \in R^{1xC}
    denom=np.sum(T) # \in R
    T=T/denom
    assert T.shape[0] == nclasses
    T[y[i]]-=1
    K=np.ones((D,nclasses))*(xi[np.newaxis,:].T)
    assert K.shape[0] == D
    assert K.shape[1] == nclasses
    K=K.T*T[np.newaxis,:].T
    assert K.shape[0] == nclasses
    assert K.shape[1] == D
    dW+=K.T
  dW/=nobs
  dW += reg*W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)
  
  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  #pass
  
  #loss = 0.0
  #dW = np.zeros_like(W)
  D,nclasses = W.shape
  assert X.shape[1] == D 
  nobs=X.shape[0]
  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  #for i in range(nobs):
  #  xi=X[i]
  #  scores=xi.dot(W)
  #  T=np.exp(scores)
  #  prob=T[y[i]]/np.sum(T)
  #  loss+=-np.log(prob)
  #pass
  scores=X.dot(W)
  scores=np.exp(scores) #\in R^{NxC}
  S=np.sum(scores, axis=1)+1e-7
  assert S.shape[0]==nobs
  probs=scores/S[np.newaxis,:].T
  probs=-np.log(probs)
  L_is=np.sum(probs[np.arange(nobs), y])
  loss+=L_is
  loss /= nobs
  loss += 0.5*reg * np.sum(W * W)
  
  #Computing gradient in looping method
  '''
  for i in range(nobs):
    xi=X[i]
    scores=xi.dot(W) # \in R^{1xC}
    T=np.exp(scores) # \in R^{1xC}
    denom=np.sum(T) # \in R
    T=T/denom
    assert T.shape[0] == nclasses
    T[y[i]]-=1
    K=np.ones((D,nclasses))*(xi[np.newaxis,:].T)
    assert K.shape[0] == D
    assert K.shape[1] == nclasses
    K=K.T*T[np.newaxis,:].T
    assert K.shape[0] == nclasses
    assert K.shape[1] == D
    dW+=K.T
  '''
  
  probs=scores/S[np.newaxis,:].T #\in R^{NxC}
  probs[np.arange(nobs), y] -= 1
  dW=probs.T.dot(X)
  dW=dW.T  
  dW/=nobs
  dW += reg*W
  
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

