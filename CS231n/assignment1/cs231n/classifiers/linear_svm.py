import numpy as np
from random import shuffle
from cs231n.gradient_check import eval_numerical_gradient
#import cs231n.gradient_check
import matplotlib.pyplot as plt
import math

def svm_loss_naive(W, X, y, reg):
  """
  Structured SVM loss function, naive implementation (with loops).

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  dW = np.zeros(W.shape) # initialize the gradient as zero

  # compute the loss and the gradient
  num_classes = W.shape[1] # C
  num_train = X.shape[0] # no observations
  loss = 0.0
  T=[0 for i in range(num_train)]
  for i in xrange(num_train):
    scores = X[i].dot(W)
    correct_class_score = scores[y[i]] #Looks weird!!!!
    T[i]=0
    for j in xrange(num_classes):
      if j == y[i]:
        continue
      margin = scores[j] - correct_class_score + 1 # note delta = 1
      if margin > 0:
        loss += margin
        T[i]+=1
     

  # Right now the loss is a sum over all training examples, but we want it
  # to be an average instead so we divide by num_train.
  loss /= num_train

  # Add regularization to the loss.
  loss += 0.5 * reg * np.sum(W * W)

  #############################################################################
  # TODO:                                                                     #
  # Compute the gradient of the loss function and store it dW.                #
  # Rather that first computing the loss and then computing the derivative,   #
  # it may be simpler to compute the derivative at the same time that the     #
  # loss is being computed. As a result you may need to modify some of the    #
  # code above to compute the gradient.                                       #
  #############################################################################
  for i in range(num_train):
    x_i=X[i]
    lab_i=y[i]
    dw_i=np.zeros(W.shape)
    S=0
    #dw_i[:,lab_i]=-x_i
    for j in range(num_classes):
      w_j=W[:,j]
      w_lab_i=W[:,lab_i]
      if j!=lab_i:
        if x_i.dot(w_j) - x_i.dot(w_lab_i)+1>0:
           dw_i[:,j]=x_i
           S+=1
    assert T[i] == S, 'Something wrong!!!!: %s vs %s' % (T[i], S)
    dw_i[:,lab_i] = -x_i*S
    assert dw_i.shape == dW.shape, 'Mismatched size'
    dW += dw_i/num_train
  dW += reg*W
  return loss, dW

def testing_grad_simple_function(X):

    #def CIFAR10_loss_fun(W):
    #    return L(X_train, Y_train, W)

    #W = np.random.rand(10, 3073) * 0.001 # random weight vector
    #df = eval_numerical_gradient(CIFAR10_loss_fun, W) # get the gradient

    assert X != None
    assert len(X.shape) == 2
    #first_instance=X[0]
    first_instance=np.array([0.1 for i in range(50)])
    #print first_instance
    #print X[0]
    #print example_function(X[0])
    #return
    step_size=0.1
    T_loss=[]
    initial_vector=first_instance
    initial_loss=example_function(initial_vector)
    T_loss.append(initial_loss)
    for i in range(20):
        grad=eval_numerical_gradient(example_function, initial_vector, verbose=False)
        initial_vector = initial_vector - step_size * grad
        loss_new=example_function(initial_vector)
        T_loss.append(loss_new)
        #assert len(grad) == len(X[0]), 'Len mismatched: %s vs %s' % (len(grad), len(X[0]))
        #x = np.arange(len(grad))
        #print x
    assert len(T_loss) == 21
    #print T_loss
    inds = np.arange(len(T_loss))
    plt.plot(inds, T_loss, 'r*-')
    plt.xlabel('Index')
    plt.ylabel('Loss value')
    plt.show()

 
def example_function(x):
    '''This function takes an vector as input and output is a real number!!!'''
    assert type(x) == np.ndarray
    assert type(x[0]) == np.float64, 'Type mismatched %s' % type(x[0])
    return -math.log(x.dot(x))
def svm_loss_vectorized(W, X, y, reg):
  """
  Structured SVM loss function, vectorized implementation.

  Inputs and outputs are the same as svm_loss_naive.
  """
  loss = 0.0
  dW = np.zeros(W.shape) # initialize the gradient as zero

  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the structured SVM loss, storing the    #
  # result in loss.                                                           #
  #############################################################################
  dW = np.zeros(W.shape) # initialize the gradient as zero
  D=W.shape[0]
  # compute the loss and the gradient
  num_classes = W.shape[1] # C
  num_train = X.shape[0] # no observations
  loss = 0.0
  #T=[0 for i in range(num_train)]
  
  
  T=X.dot(W)
  correct_class_score = T[np.arange(num_train), y] #Qua suc ba dao!!!!!!!!
  #print correct_class_score.shape
  temp=np.ones(num_classes)
  temp=temp[np.newaxis,:]
  correct_class_score = (correct_class_score[np.newaxis,:].T).dot(temp)
  M=T-correct_class_score+1
  loss=np.sum(M[M>0])-num_train
  
  # Right now the loss is a sum over all training examples, but we want it
  # to be an average instead so we divide by num_train.
  loss /= num_train

  # Add regularization to the loss.
  loss += 0.5 * reg * np.sum(W * W)
  
  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the gradient for the structured SVM     #
  # loss, storing the result in dW.                                           #
  #                                                                           #
  # Hint: Instead of computing the gradient from scratch, it may be easier    #
  # to reuse some of the intermediate values that you used to compute the     #
  # loss.                                                                     #
  #############################################################################
  #pass
  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################
  #pass
  '''First version of vectorized gradient!!!'''
  '''
  for i in range(num_train):
    x_i=X[i]
    lab_i=y[i]
    dw_i=np.zeros(W.shape)
    #S=0
    #dw_i[:,lab_i]=-x_i
    scores=x_i.dot(W)
    correct_score=scores[lab_i]
    scores=scores-correct_score+1
    inds=np.arange(scores.shape[0])
    non_negative_inds=inds[scores>0]
    S=len(non_negative_inds)-1
    temp=x_i[np.newaxis,:]
    temp1=np.ones((1,len(non_negative_inds)))

    K=temp.T.dot(temp1)
    
    dw_i[:, non_negative_inds]=K
    dw_i[:,lab_i]=-x_i*S

    assert dw_i.shape == dW.shape, 'Mismatched size'
    dW += dw_i/num_train
  '''
  '''Second version of vectorized gradient!!!'''
  T=X.dot(W)
  #print T
  #correct_scores=T[:,y]
  #print y.shape, 'y shape'
  #print correct_scores
  #print correct_scores.shape, 'what'
  
  correct_scores=T[np.arange(num_train),y] # \in R^N 
  correct_scores=correct_scores[np.newaxis,:]
  #correct_scores
  #print correct_scores.shape, 'second'
  
  #raise Exception('Stop here')
  M=T-correct_scores.T+1 # \in NxC
  #M=T-correct_scores.T.dot(np.ones((1,num_classes)))+1 #slow
  assert M.shape[0] == num_train, 'shape of M: %s' % M.shape[0] 
  assert M.shape[1] == num_classes
  M[np.arange(num_train),y]=0
  M=np.where(M>0,1,0)
  #print M.shape, 'what the heck'
  #M[:,y]=0 #lay pair vay (row, col), not full matrix,
  
  #print temp
  #assert temp.shape[0] == num_train, temp.shape
  #assert temp.shape[1] == 1
  
  temp=-1*np.sum(M,axis=1)
  #print temp
  assert temp.shape[0] == num_train
  M[np.arange(num_train),y]=temp #\in NxC
  K=M.T.dot(X) #CxN dot NxD => CxD
  dW=K.T
  dW/=num_train
  dW += reg*W
  #pass
  return loss, dW
