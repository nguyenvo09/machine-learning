import numpy as np


"""
This file defines layer types that are commonly used for recurrent neural
networks.
"""


def rnn_step_forward(x, prev_h, Wx, Wh, b):
  """
  Run the forward pass for a single timestep of a vanilla RNN that uses a tanh
  activation function.

  The input data has dimension D, the hidden state has dimension H, and we use
  a minibatch size of N.

  Inputs:
  - x: Input data for this timestep, of shape (N, D).
  - prev_h: Hidden state from previous timestep, of shape (N, H)
  - Wx: Weight matrix for input-to-hidden connections, of shape (D, H)
  - Wh: Weight matrix for hidden-to-hidden connections, of shape (H, H)
  - b: Biases of shape (H,)

  Returns a tuple of:
  - next_h: Next hidden state, of shape (N, H)
  - cache: Tuple of values needed for the backward pass.
  """
  next_h, cache = None, None
  ##############################################################################
  # TODO: Implement a single forward step for the vanilla RNN. Store the next  #
  # hidden state and any values you need for the backward pass in the next_h   #
  # and cache variables respectively.                                          #
  ##############################################################################
  N, D = x.shape
  _, H = prev_h.shape
  A = x.dot(Wx) #shape(N,H)
  B = prev_h.dot(Wh) #shape (N,H)
  next_h = np.tanh(A + B + b)
  assert next_h.shape == (N, H)
  cache = (next_h, x, prev_h, Wx, Wh, b, N, D, H)
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return next_h, cache


def rnn_step_backward(dnext_h, cache):
  """
  Backward pass for a single timestep of a vanilla RNN.
  
  Inputs:
  - dnext_h: Gradient of loss with respect to next hidden state
  - cache: Cache object from the forward pass
  
  Returns a tuple of:
  - dx: Gradients of input data, of shape (N, D)
  - dprev_h: Gradients of previous hidden state, of shape (N, H)
  - dWx: Gradients of input-to-hidden weights, of shape (N, H)
  - dWh: Gradients of hidden-to-hidden weights, of shape (H, H)
  - db: Gradients of bias vector, of shape (H,)
  """
  dx, dprev_h, dWx, dWh, db = None, None, None, None, None
  ##############################################################################
  # TODO: Implement the backward pass for a single step of a vanilla RNN.      #
  #                                                                            #
  # HINT: For the tanh function, you can compute the local derivative in terms #
  # of the output value from tanh.                                             #
  ##############################################################################
  next_h, x, prev_h, Wx, Wh, b, N, D, H = cache
  assert dnext_h.shape == (N, H)
  dnext_h = np.multiply(dnext_h, 1.0 - np.multiply(next_h, next_h))
  db = np.sum(dnext_h, axis=0)
  dWh = dnext_h.T.dot(prev_h)
  assert dWh.shape == (H, H)
  dWh = dWh.T
  dWx = x.T.dot(dnext_h)
  dprev_h = dnext_h.dot(Wh.T) #(H, H) may be wrong
  dx = dnext_h.dot(Wx.T) #(N, D)
  #pass
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return dx, dprev_h, dWx, dWh, db


def rnn_forward(x, h0, Wx, Wh, b):
  """
  Run a vanilla RNN forward on an entire sequence of data. We assume an input
  sequence composed of T vectors, each of dimension D. The RNN uses a hidden
  size of H, and we work over a minibatch containing N sequences. After running
  the RNN forward, we return the hidden states for all timesteps.
  
  Inputs:
  - x: Input data for the entire timeseries, of shape (N, T, D).
  - h0: Initial hidden state, of shape (N, H)
  - Wx: Weight matrix for input-to-hidden connections, of shape (D, H)
  - Wh: Weight matrix for hidden-to-hidden connections, of shape (H, H)
  - b: Biases of shape (H,)
  
  Returns a tuple of:
  - h: Hidden states for the entire timeseries, of shape (N, T, H).
  - cache: Values needed in the backward pass
  """
  h, cache = None, None
  ##############################################################################
  # TODO: Implement forward pass for a vanilla RNN running on a sequence of    #
  # input data. You should use the rnn_step_forward function that you defined  #
  # above.                                                                     #
  ##############################################################################
  # pass
  N, T, D = x.shape
  prev_h = h0
  _, H = h0.shape
  cache = []
  h = []
  for i in xrange(T):
      data = x[:, i, :]
      next_h, cache1 = rnn_step_forward(data, prev_h, Wx, Wh, b)
      prev_h = next_h
      cache.append(cache1)
      h.append(next_h)
  h = np.array(h)
  assert h.shape == (T, N, H)
  # print h.shape
  h = np.swapaxes(h,0, 1)
  #print h.shape
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return h, cache


def rnn_backward(dh, cache):
  """
  Compute the backward pass for a vanilla RNN over an entire sequence of data.
  
  Inputs:
  - dh: Upstream gradients of all hidden states, of shape (N, T, H)
  ???? Why upstream gradients of all hidden states???? I thought we only need upstream gradient of the last hidden state???
  Returns a tuple of:
  - dx: Gradient of inputs, of shape (N, T, D)
  - dh0: Gradient of initial hidden state, of shape (N, H)
  - dWx: Gradient of input-to-hidden weights, of shape (D, H)
  - dWh: Gradient of hidden-to-hidden weights, of shape (H, H)
  - db: Gradient of biases, of shape (H,)
  """
  dx, dh0, dWx, dWh, db = None, None, None, None, None
  ##############################################################################
  # TODO: Implement the backward pass for a vanilla RNN running an entire      #
  # sequence of data. You should use the rnn_step_backward function that you   #
  # defined above.                                                             #
  ##############################################################################
  # pass
  N, T, H = dh.shape
  _, _, _, _, _, _, N, D, H = cache[0]
  # T = len(cache)
  dx = np.zeros((N, T, D))
  db = np.zeros(H,)
  dWh = np.zeros((H, H))
  dWx = np.zeros((D, H))

  dprev_h = 0.0
  for i in xrange(T-1, -1, -1):
      # print 'index: ', i
      cache_data = cache[i]
      dnext_h = dh[:, i, :] + dprev_h #The weirdest I have seen in my life!!!!
      dx1, dprev_h, dWx1, dWh1, db1 = rnn_step_backward(dnext_h, cache_data)
      assert dx1.shape == (N, D)
      dx[:, i, :] = dx1

      dWx += dWx1
      dWh += dWh1
      db += db1

      if i == 0:
          dh0 = dprev_h
  # dh0 = dnext_h
  # dx = np.array(dx)
  # dx = np.swapaxes(dx, 0, 1)

  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return dx, dh0, dWx, dWh, db


def word_embedding_forward(x, W):
  """
  Forward pass for word embeddings. We operate on minibatches of size N where
  each sequence has length T. We assume a vocabulary of V words, assigning each
  to a vector of dimension D.
  
  Inputs:
  - x: Integer array of shape (N, T) giving indices of words. Each element idx
    of x must be in the range 0 <= idx < V.
  - W: Weight matrix of shape (V, D) giving word vectors for all words.
  
  Returns a tuple of:
  - out: Array of shape (N, T, D) giving word vectors for all input words.
  - cache: Values needed for the backward pass
  """
  out, cache = None, None
  ##############################################################################
  # TODO: Implement the forward pass for word embeddings.                      #
  #                                                                            #
  # HINT: This should be very simple. (useless hint!!!)
  # (2) Same word must have same embedding vector
  #  Don gian la co word vector storing trong W roi, cu lay ra roi
  #                                      #
  ##############################################################################
  # pass
  N, T = x.shape
  V, D = W.shape
  out = []
  for i in xrange(N):
      sentence = x[i] #shape = (T, ) list of indices, there may be duplication because a sentence may have multiple same words
      out.append(W[sentence]) # shape (T, D) selecting from matrix W the rows corresponding to indices of the above sentence
  out = np.array(out)
  assert out.shape == (N, T, D)
  cache = (W, x)
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return out, cache


def word_embedding_backward(dout, cache):
  """
  Backward pass for word embeddings. We cannot back-propagate into the words
  since they are integers, so we only return gradient for the word embedding
  matrix.
  
  HINT: Look up the function np.add.at
  
  Inputs:
  - dout: Upstream gradients of shape (N, T, D)
  - cache: Values from the forward pass
  
  Returns:
  - dW: Gradient of word embedding matrix, of shape (V, D).
  """
  N, T, D = dout.shape
  W, x = cache
  V, D = W.shape
  dW = np.zeros((V, D))
  ##############################################################################
  # TODO: Implement the backward pass for word embeddings.                     #
  #                                                                            #
  # HINT: Look up the function np.add.at                                       #
  ##############################################################################
  # pass
  for i in xrange(N):
      sentence = x[i] #shape = (T, ) list of indices, there may be duplication because a sentence may have multiple same words
      np.add.at(dW, sentence, dout[i, :, :])

  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  return dW


def sigmoid(x):
  """
  A numerically stable version of the logistic sigmoid function.
  """
  pos_mask = (x >= 0)
  neg_mask = (x < 0)
  z = np.zeros_like(x)
  z[pos_mask] = np.exp(-x[pos_mask])
  z[neg_mask] = np.exp(x[neg_mask])
  top = np.ones_like(x)
  top[neg_mask] = z[neg_mask]
  return top / (1 + z)


def lstm_step_forward(x, prev_h, prev_c, Wx, Wh, b):
  """
  Forward pass for a single timestep of an LSTM.
  
  The input data has dimension D, the hidden state has dimension H, and we use
  a minibatch size of N.
  
  Inputs:
  - x: Input data, of shape (N, D)
  - prev_h: Previous hidden state, of shape (N, H)
  - prev_c: previous cell state, of shape (N, H)
  - Wx: Input-to-hidden weights, of shape (D, 4H)
  - Wh: Hidden-to-hidden weights, of shape (H, 4H)
  - b: Biases, of shape (4H,)
  
  Returns a tuple of:
  - next_h: Next hidden state, of shape (N, H)
  - next_c: Next cell state, of shape (N, H)
  - cache: Tuple of values needed for backward pass.
  """
  next_h, next_c, cache = None, None, None
  #############################################################################
  # TODO: Implement the forward pass for a single timestep of an LSTM.        #
  # You may want to use the numerically stable sigmoid implementation above.  #
  #############################################################################
  # pass
  N, D = x.shape
  D, H4 = Wx.shape
  H, H4 = Wh.shape
  # print H4, H
  assert H4 == 4*H
  a = x.dot(Wx) + prev_h.dot(Wh) + b
  assert a.shape == (N, 4*H)
  i = a[:, :H]
  f = a[:, H: 2*H]
  o = a[:, 2*H: 3*H]
  g = a[:, 3*H:]

  i = sigmoid(i)
  f = sigmoid(f)
  o = sigmoid(o)
  g = np.tanh(g)

  next_c = np.multiply(f, prev_c) + np.multiply(i, g)
  K = np.tanh(next_c)
  next_h = np.multiply(o, K)

  cache = (x, prev_h, prev_c, Wx, Wh, b, K, i, f, o, g, a)
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  
  return next_h, next_c, cache


def lstm_step_backward(dnext_h, dnext_c, cache):
  """
  Backward pass for a single timestep of an LSTM.
  
  Inputs:
  - dnext_h: Gradients of next hidden state, of shape (N, H)
  - dnext_c: Gradients of next cell state, of shape (N, H)
  - cache: Values from the forward pass
  
  Returns a tuple of:
  - dx: Gradient of input data, of shape (N, D)
  - dprev_h: Gradient of previous hidden state, of shape (N, H)
  - dprev_c: Gradient of previous cell state, of shape (N, H)
  - dWx: Gradient of input-to-hidden weights, of shape (D, 4H)
  - dWh: Gradient of hidden-to-hidden weights, of shape (H, 4H)
  - db: Gradient of biases, of shape (4H,)
  """
  dx, dprev_h, dprev_c, dWx, dWh, db = None, None, None, None, None, None
  #############################################################################
  # TODO: Implement the backward pass for a single timestep of an LSTM.       #
  #                                                                           #
  # HINT: For sigmoid and tanh you can compute local derivatives in terms of  #
  # the output value from the nonlinearity.                                   #
  #############################################################################
  # pass
  N, H = dnext_h.shape

  x, prev_h, prev_c, Wx, Wh, b, K, i, f, o, g, a = cache
  N, D = x.shape
  do = K * dnext_h
  dnext_c_from_next_h = dnext_h * o * (1.0 - K*K)

  dnext_c += dnext_c_from_next_h
  dprev_c = f * dnext_c

  df =   prev_c * dnext_c
  di = g * dnext_c
  dg = i * dnext_c

  # db = np.zeros(4*H, )
  # dWh = np.zeros((H, 4*H))
  # dprev_h = np.zeros((N, H))
  # dWx = np.zeros((D, 4*H))
 # dprev_c = np.zeros((N, H))
 #  dx = np.zeros((N, D))
  #i,f,o,g
  partB = np.zeros((N, 4*H))
  partB[:, :H] = di * i * (1.0-i)
  partB[:, H:2*H] = df * f * (1.0-f)
  partB[:, 2*H:3*H] = do * o * (1.0-o)
  partB[:, 3*H: 4*H] = dg * (1.0-g*g)

  #########################################################
  # print di.shape, i.shape
  db = np.sum(partB, axis=0)
  ################################################################
  dWh = prev_h.T.dot(partB)
  dWx = x.T.dot(partB)
  dprev_h = partB.dot(Wh.T)
  dx = partB.dot(Wx.T)
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################

  return dx, dprev_h, dprev_c, dWx, dWh, db


def lstm_forward(x, h0, Wx, Wh, b):
  """
  Forward pass for an LSTM over an entire sequence of data. We assume an input
  sequence composed of T vectors, each of dimension D. The LSTM uses a hidden
  size of H, and we work over a minibatch containing N sequences. After running
  the LSTM forward, we return the hidden states for all timesteps.
  
  Note that the initial cell state is passed as input, but the initial cell
  state is set to zero. Also note that the cell state is not returned; it is
  an internal variable to the LSTM and is not accessed from outside.
  
  Inputs:
  - x: Input data of shape (N, T, D)
  - h0: Initial hidden state of shape (N, H)
  - Wx: Weights for input-to-hidden connections, of shape (D, 4H)
  - Wh: Weights for hidden-to-hidden connections, of shape (H, 4H)
  - b: Biases of shape (4H,)
  
  Returns a tuple of:
  - h: Hidden states for all timesteps of all sequences, of shape (N, T, H)
  - cache: Values needed for the backward pass.
  """
  h, cache = None, None
  #############################################################################
  # TODO: Implement the forward pass for an LSTM over an entire timeseries.   #
  # You should use the lstm_step_forward function that you just defined.      #
  #############################################################################
  # pass
  N, T, D = x.shape
  N, H = h0.shape
  prev_h = h0
  prev_c = 0
  h = []
  cache = []
  for i in xrange(T):
      input = x[:, i, :]
      next_h, next_c, cache1 = lstm_step_forward(input, prev_h, prev_c, Wx, Wh, b)
      prev_h = next_h
      prev_c = next_c
      h.append(next_h)
      cache.append(cache1)
  h = np.array(h)
  h = np.swapaxes(h, 0, 1)
  # print h.shape
  assert h.shape == (N, T, H)
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################

  return h, cache


def lstm_backward(dh, cache):
  """
  Backward pass for an LSTM over an entire sequence of data.]
  
  Inputs:
  - dh: Upstream gradients of hidden states, of shape (N, T, H)
  - cache: Values from the forward pass
  
  Returns a tuple of:
  - dx: Gradient of input data of shape (N, T, D)
  - dh0: Gradient of initial hidden state of shape (N, H)
  - dWx: Gradient of input-to-hidden weight matrix of shape (D, 4H)
  - dWh: Gradient of hidden-to-hidden weight matrix of shape (H, 4H)
  - db: Gradient of biases, of shape (4H,)
  """
  dx, dh0, dWx, dWh, db = None, None, None, None, None
  #############################################################################
  # TODO: Implement the backward pass for an LSTM over an entire timeseries.  #
  # You should use the lstm_step_backward function that you just defined.     #
  #############################################################################
  # pass
  N, T, H = dh.shape
  initial_input, _, _, _, _, _, _, _, _, _, _, _ = cache[0]
  N, D = initial_input.shape
  #initialization
  dx = np.zeros((N, T, D))
  dWx = np.zeros((D, 4*H))
  dWh = np.zeros((H, 4*H))
  db = np.zeros(4*H, )
  # init_dnext_h = 0.0
  # init_dnext_c = 0.0
  # dnext_h = dh[:, T-1, :]
  dnext_c = 0.0
  dprev_h = 0.0
  for i in xrange(T - 1, -1, -1):
      # print 'here'
      cache_data = cache[i]
      dnext_h = dh[:, i, :] + dprev_h

      dx1, dprev_h, dprev_c, dWx1, dWh1, db1 = lstm_step_backward(dnext_h, dnext_c, cache_data)
      dnext_c = dprev_c
      dx[:, i, :] = dx1
      db += db1
      dWh += dWh1
      dWx += dWx1
      if i == 0:
          dh0 = dprev_h
  ##############################################################################
  #                               END OF YOUR CODE                             #
  ##############################################################################
  
  return dx, dh0, dWx, dWh, db


def temporal_affine_forward(x, w, b):
  """
  Forward pass for a temporal affine layer. The input is a set of D-dimensional
  vectors arranged into a minibatch of N timeseries, each of length T. We use
  an affine function to transform each of those vectors into a new vector of
  dimension M.

  Inputs:
  - x: Input data of shape (N, T, D)
  - w: Weights of shape (D, M)
  - b: Biases of shape (M,)
  
  Returns a tuple of:
  - out: Output data of shape (N, T, M)
  - cache: Values needed for the backward pass
  """
  N, T, D = x.shape
  M = b.shape[0]
  out = x.reshape(N * T, D).dot(w).reshape(N, T, M) + b
  cache = x, w, b, out
  return out, cache


def temporal_affine_backward_ben(dout, cache):
    """
    Ben's version Backward pass for temporal affine layer.
    Input:
    - dout: Upstream gradients of shape (N, T, M)
    - cache: Values from forward pass
    :param dout:
    :param cache:
    :return: Returns a tuple of:
    - dx: Gradient of input, of shape (N, T, D)
    - dw: Gradient of weights, of shape (D, M)
    - db: Gradient of biases, of shape (M,)
    """
    x, w, b, out = cache
    N, T, M = dout.shape
    D, M = w.shape
    db = np.sum(dout.reshape(N*T, M), axis=0)
    dw = dout.reshape(N*T, M).T.dot(x.reshape(N*T, D))
    dw = dw.T
    dx = dout.reshape(N*T, M).dot(w.T).reshape(N,T,D)
    return dx, dw, db

def temporal_affine_backward(dout, cache):
  """
  Backward pass for temporal affine layer.

  Input:
  - dout: Upstream gradients of shape (N, T, M)
  - cache: Values from forward pass

  Returns a tuple of:
  - dx: Gradient of input, of shape (N, T, D)
  - dw: Gradient of weights, of shape (D, M)
  - db: Gradient of biases, of shape (M,)
  """
  x, w, b, out = cache
  N, T, D = x.shape
  M = b.shape[0]

  dx = dout.reshape(N * T, M).dot(w.T).reshape(N, T, D)
  dw = dout.reshape(N * T, M).T.dot(x.reshape(N * T, D)).T
  db = dout.sum(axis=(0, 1))

  return dx, dw, db


def temporal_softmax_loss_ben(x, y, mask, verbose=False):
    """
    Ben's version, implement it to have better understanding!!!!!
    A temporal version of softmax loss for use in RNNs. We assume that we are
    making predictions over a vocabulary of size V for each timestep of a
    timeseries of length T, over a minibatch of size N. The input x gives scores
    for all vocabulary elements at all timesteps, and y gives the indices of the
    ground-truth element at each timestep. We use a cross-entropy loss at each
    timestep, summing the loss over all timesteps and averaging across the
    minibatch.

    As an additional complication, we may want to ignore the model output at some
    timesteps, since sequences of different length may have been combined into a
    minibatch and padded with NULL tokens. The optional mask argument tells us
    which elements should contribute to the loss.

    Inputs:
    - x: Input scores, of shape (N, T, V)
    Explanation is not clear dude!!!!,
    https://www.youtube.com/watch?v=Keqep_PKrY8&list=PL3FW7Lu3i5Jsnh1rnUwq_TcylNr7EkRe6&index=8 is more clear.
    The state h_t output from RNN cell t^{th} has shape (N,H). This h_t will be inputted into a fully connected layer
     W_S which have shape (H, V). The output of h_t.dot(W_S) is a N x V matrix. This matrix will then inputted to softmax layer
     to create a matrix (N x V) dimension. Because we have T RNN cells, the output score fromm T RNN cells has shape (N, T, V)
     The matrix (N x V) above, at each row, we select the max probability to know what word will be predicted.

     It looks like unsupervised learning, but we create supervised learning by using label the word next to each other.
     See     https://www.youtube.com/watch?v=Keqep_PKrY8&list=PL3FW7Lu3i5Jsnh1rnUwq_TcylNr7EkRe6&index=8 is more clear.
     for more details !!!!!


    - y: Ground-truth indices, of shape (N, T) where each element is in the range
         0 <= y[i, t] < V
    - mask: Boolean array of shape (N, T) where mask[i, t] tells whether or not
      the scores at x[i, t] should contribute to the loss.

    Returns a tuple of:
    - loss: Scalar giving loss
    - dx: Gradient of loss with respect to scores x.
    """
    N, T, V = x.shape
    dx = np.zeros((N, T, V))
    #(1) We need to input x matrix to softmax layer which has shape (V, )
    K1 = x.reshape(N*T, V).T  #transform data into form (N, D) then input it to softmax layer
    assert K1.shape == (V, N*T)
    #(2) find max element each row of K. I do this to make "numerical stability"
    K2 = K1 - np.max(K1, axis=0)

    # (3) find softmax, we have probability already.
    K = np.exp(K2)
    K /= np.sum(K, axis=0)
    K = K.T
    assert K.shape == (N*T, V)
    #Tinh gradient at this step
    dx = dx.reshape(N * T, V)
    dx[np.arange(N * T), y.flatten()] = 1.0
    dx = -1.0/N * (dx - K).T * mask.flatten()
    dx = dx.T.reshape(N, T, V)
    #Done, du ma, store ngay vao latex file!!!
    #assert abs(np.sum(np.sum(K, axis=1) - np.ones(N*T))) < 1e-10

    loss = np.sum(-np.log(K[np.arange(N*T), y.flatten()].T) * mask.flatten()) / float(N)




    return loss, dx

def temporal_softmax_loss(x, y, mask, verbose=False):
  """
  A temporal version of softmax loss for use in RNNs. We assume that we are
  making predictions over a vocabulary of size V for each timestep of a
  timeseries of length T, over a minibatch of size N. The input x gives scores
  for all vocabulary elements at all timesteps, and y gives the indices of the
  ground-truth element at each timestep. We use a cross-entropy loss at each
  timestep, summing the loss over all timesteps and averaging across the
  minibatch.

  As an additional complication, we may want to ignore the model output at some
  timesteps, since sequences of different length may have been combined into a
  minibatch and padded with NULL tokens. The optional mask argument tells us
  which elements should contribute to the loss.

  Inputs:
  - x: Input scores, of shape (N, T, V)
  - y: Ground-truth indices, of shape (N, T) where each element is in the range
       0 <= y[i, t] < V
  - mask: Boolean array of shape (N, T) where mask[i, t] tells whether or not
    the scores at x[i, t] should contribute to the loss.

  Returns a tuple of:
  - loss: Scalar giving loss
  - dx: Gradient of loss with respect to scores x.
  """

  N, T, V = x.shape
  
  x_flat = x.reshape(N * T, V)
  y_flat = y.reshape(N * T)
  mask_flat = mask.reshape(N * T)
  
  probs = np.exp(x_flat - np.max(x_flat, axis=1, keepdims=True))
  probs /= np.sum(probs, axis=1, keepdims=True)
  loss = -np.sum(mask_flat * np.log(probs[np.arange(N * T), y_flat])) / N
  dx_flat = probs.copy()
  dx_flat[np.arange(N * T), y_flat] -= 1
  dx_flat /= N
  dx_flat *= mask_flat[:, None]
  
  if verbose: print 'dx_flat: ', dx_flat.shape
  
  dx = dx_flat.reshape(N, T, V)
  
  return loss, dx

